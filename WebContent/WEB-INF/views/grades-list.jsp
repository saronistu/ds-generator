<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<body>
	<div class="container">
		<h1>Grades of student ${numeStudent}</h1>
		<h3>Media generală: <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${medie}" /></h3>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Materie</th>
					<th>Ore C:</th>
					<th>Ore SLPP:</th>
					<th>Nota sem 1</th>
					<th>Nota sem 2</th>
					<th>Credite sem 1</th>
					<th>Credite sem 2</th>
					<th>An universitar</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<x:forEach items="${enrollmentsList}" var="enrollment" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${enrollment.nume}</td>
				<td>${enrollment.oreC}</td>
				<td>${enrollment.oreSLPP}</td>
				<td>${enrollment.notaSem1}</td>
				<td>${enrollment.notaSem2}</td>
				<td>${enrollment.crediteSem1}</td>
				<td>${enrollment.crediteSem2}</td>
				<td>${enrollment.anUniversitar}</td>
				<td>
					<a href="edit-grade?id=${enrollment.id}" class="btn btn-primary">Edit</a>
					<a href="delete-grade?id=${enrollment.id}" class="btn btn-danger">Delete</a>
				</td>
			</tr>
			</x:forEach>
			</tbody>
		</table>
			<a class="btn btn-success" href="enrol-student?id=${id}">Add</a>
	</div>
<%@ include file="common/footer.jspf" %>