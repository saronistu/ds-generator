<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>List students by department and year</h1>
		<div class="row">
			<div class="col">
				<h4>Group by registration year</h4>
				<form:form action="group-list" method="post" modelAttribute="department">
					<div class="form-group">
						<label>Specializare</label>
						<form:select path="nume" type="text" class="form-control">
							<form:options items="${allDepartments}" />
						</form:select>
					</div>
					<div class="form-group">
						<label>Anul înmatriculării</label>
						<form:input path="an" type="number" class="form-control" value="2017"/>
					</div>
					<input type="submit" class="btn btn-sitecolor" value="View">
				</form:form>
			</div>
			<div class="col">
				<h4>Group by graduation year</h4>
				<form:form action="group-list-grad" method="post" modelAttribute="department">
					<div class="form-group">
						<label>Specializare</label>
						<form:select path="nume" type="text" class="form-control">
							<form:options items="${allDepartments}" />
						</form:select>
					</div>
					<div class="form-group">
						<label>Anul înmatriculării</label>
						<form:input path="an" type="number" class="form-control" value="2017"/>
					</div>
					<input type="submit" class="btn btn-sitecolor" value="View">
				</form:form>
			</div>
		</div>

	</div>
<%@ include file="common/footer.jspf" %>