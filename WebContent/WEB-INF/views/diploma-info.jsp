<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<div class="container">
	<h1>Edit student: information regarding the diploma</h1>
	<form:form action="save-diploma" method="post" modelAttribute="diploma">
		<form:hidden path="id" />
		<div class="form-group">
			<label>Serie diplomă</label>
			<form:input path="serie" type="text" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Număr diplomă</label>
			<form:input path="numar" type="text" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Informații suplimentare</label>
			<form:textarea path="informatiiSuplimentare" rows="2" type="text" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Număr pagini</label>
			<form:input path="numarPagini" type="number" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Data eliberării</label>
			<form:input path="dataEliberarii" type="text" class="form-control"/>
		</div>
		<input type="submit" class="btn btn-sitecolor" value="Save">
	</form:form>
</div>
<%@ include file="common/footer.jspf" %>