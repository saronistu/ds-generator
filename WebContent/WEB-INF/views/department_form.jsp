<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Edit department: ${department.nume}</h1>
		<form:form action="save-department?id=${department.id}" method="post" modelAttribute="department">
			<form:hidden path="id" />
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label>Denumirea și titlul calificării</label>
						<form:input path="denumireTitluCalificare" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Domeniul de studii</label>
						<form:input path="nume" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Programul de studii</label>
						<form:input path="programStudii" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Numele instituției care eliberează diploma</label>
						<form:textarea path="numeInstitutie" rows="2" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Facultatea care organizează examenul</label>
						<form:input path="facultate" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Instituția care a asigurat școlarizarea</label>
						<form:input path="numeInstitutieDifera" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Facultatea care a asigurat școlarizarea (dacă diferă)</label>
						<form:input path="facultateDifera" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Limba de studiu</label>
						<form:input path="limbaStudiu" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Nivelul calificării</label>
						<form:input path="nivelCalificare" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Durata oficială a programului</label>
						<form:input path="durataProgramului" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Condițiile de admitere</label>
						<form:input path="conditiiAdmitere" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Forma de învățământ</label>
						<form:input path="formaInvatamant" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Rezultatele învățării asigurate prin programul de studii</label>
						<form:textarea path="rezultateleInvatarii" rows="15" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Posibilități de continuare a studiilor</label>
						<form:input path="posibilitateStudii" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Statutul profesional</label>
						<form:textarea path="statutProfesional" rows="8" type="text" class="form-control"/>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label>Name of qualification and title </label>
						<form:input path="denumireTitluCalificareEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Field of study</label>
						<form:input path="numeEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Programme of study</label>
						<form:input path="programStudiiEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Name and status of awarding institution</label>
						<form:textarea path="numeInstitutieEngleza" rows="2" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Faculty administering the final examination</label>
						<form:input path="facultateEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Name and status of institution administering studies</label>
						<form:input path="numeInstitutieDiferaEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Faculty administering studies if different</label>
						<form:input path="facultateDiferaEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Language(s) of instruction</label>
						<form:input path="limbaStudiuEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Level of qualification</label>
						<form:input path="nivelCalificareEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Official length of the programme</label>
						<form:input path="durataProgramuluiEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Access requirement(s)</label>
						<form:input path="conditiiAdmitereEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Mode of study</label>
						<form:input path="formaInvatamantEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Learning outcomes of th study programme</label>
						<form:textarea path="rezultateleInvatariiEngleza" rows="15" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Access to further study</label>
						<form:input path="posibilitateStudiiEngleza" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Professional status</label>
						<form:textarea path="statutProfesionalEngleza" rows="8" type="text" class="form-control"/>
					</div>
				</div>
			</div>
			<input type="submit" class="btn btn-success" value="Save">
			</div>
		</form:form>
	</div>
<%@ include file="common/footer.jspf" %>