<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Add a subject</h1>
		<form:form action="add-subject" method="post" modelAttribute="subject">
			<div class="form-group">
				<label>Nume:</label>
				<form:input path="name" type="text" class="form-control"/>
			</div>
			<div class="form-group">
				<label>Nume în limba engleză:</label>
				<form:input path="englishName" type="text" class="form-control"/>
			</div>
			<input type="submit" class="btn btn-sitecolor" value="Add">
		</form:form>
	</div>
<%@ include file="common/footer.jspf" %>