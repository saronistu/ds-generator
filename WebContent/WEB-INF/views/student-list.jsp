<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<body>
	<div class="container">
		<h1>Students list</h1>
		<table class="table table-striped table-hover">
			<thead class="thead-light">
				<tr>
					<th class="hide-me">#</th>
					<th>First name</th>
					<th>Last name</th>
					<th class="hide-me">Date of birth</th>
					<th class="hide-me">Enrolment #</th>
					<th class="hide-me">Field of study</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<x:forEach items="${listStudent}" var="student">
			<tr>
				<td class="hide-me">${student.id}</td>
				<td>${student.prenume}</td>
				<td>${student.nume}</td>
				<td class="hide-me">${student.anulNasterii}-${student.lunaNasterii}-${student.ziuaNasterii}</td>
				<td class="hide-me">${student.numarMatricol}</td>
				<td class="hide-me">${student.domeniuStudii}</td>
				<td>
					<security:authorize access="hasAuthority('ADMIN')">
					<a href="edit?id=${student.id}" class="btn btn-primary">Edit</a>
					<a href="delete?id=${student.id}" class="btn btn-danger">Delete</a>
					<a href="grades-list?id=${student.id}" class="btn btn-success">Grades</a>
					<a href="duplicate?id=${student.id}" class="btn btn-secondary">Duplicate</a>
					</security:authorize>
					<a href="generate?id=${student.id}" class="btn btn-info" target="_blank">Generate</a>
				</td>
			</tr>
			</x:forEach>
			</tbody>
		</table>
	</div>
<%@ include file="common/footer.jspf" %>