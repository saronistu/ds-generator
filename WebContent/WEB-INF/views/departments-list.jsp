<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<body>
	<div class="container">
		<h1>Departments list</h1>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="hide-me">#</th>
					<th>Name</th>
					<th>Facultate</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<x:forEach items="${departmentsList}" var="department" varStatus="status">
			<tr>
				<td class="hide-me">${status.index + 1}</td>
				<td>${department.nume}</td>
				<td>${department.facultate}</td>
				<td>
					<a href="edit-department?id=${department.id}" class="btn btn-primary">Edit</a>
					<a href="delete-department?id=${department.id}" class="btn btn-danger">Delete</a>
				</td>
			</tr>
			</x:forEach>
			</tbody>
		</table>
	</div>
<%@ include file="common/footer.jspf" %>