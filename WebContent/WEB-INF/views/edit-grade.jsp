<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Edit grade of student</h1>
			<form:form action="save-grade?id=${enrollments.id}" method="post" modelAttribute="enrollments">
				<form:hidden path="id"/>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label>Materie:</label>
							<form:select path="nume" class="form-control">
								<form:options items="${allSubjects}" />
							</form:select>
						</div>
						<div class="form-group">
							<label>Ore C:</label>
							<form:input path="oreC" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Ore SLPP:</label>
							<form:input path="oreSLPP" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Nota sem 1:</label>
							<form:input path="notaSem1" type="text" class="form-control"/>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label>Nota sem 2:</label>
							<form:input path="notaSem2" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Credite sem 1:</label>
							<form:input path="crediteSem1" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Credite sem 2:</label>
							<form:input path="crediteSem2" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>An universitar:</label>
							<form:input path="anUniversitar" type="text" class="form-control"/>
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-sitecolor" value="Save">
			</form:form>
	</div>
<%@ include file="common/footer.jspf" %>