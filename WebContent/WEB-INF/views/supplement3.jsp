<%@ include file="common/header.jspf" %>
<section>
	<div class="text-align-center">
		<h2 class="bold">${metadata.country}</h2>
		<h4 class="bold">${metadata.ministerRomana}</h4>
		<h4 class="italic-text less-font-weight">${metadata.ministerEngleza}</h4>
		<h4 class="top-bottom-paddings">${metadata.numeInstitutie}<sup>1)</sup></h4>
		<h4 class="less-font-weight">${metadata.numeDocument}</h4>
		<h4 class="italic-text less-font-weight">${metadata.numeDocumentEngleza}</h4>
	</div>
	<div class="display-flex">
		<div class="give-me-width"></div>
		<div class="serie-diploma">
			<p><sup>2)</sup>Acest supliment însoțește diploma</p>
			<div class="display-flex">
				<p>cu seria</p><p class="serial-number">${diploma.serie}</p><p>nr. </p><p class="serial-number">${diploma.numar}</p>
			</div>
			<p>The Supplement is for diploma</p>
			<div class="display-flex">
				<p>series</p><p class="serial-number">${diploma.serie}</p><p>no. </p><p class="serial-number">${diploma.numar}</p>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-1">
	<div class="container">
		<div class="text-align-center">
			<h4>1. DATE DE IDENTIFICARE A TITULARULUI DIPLOMEI</h4>
			<h4 class="italic-text">INFORMATION IDENTIFYING THE HOLDER OF THE DIPLOMA</h4>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<p>Nume de familie la naștere</p>
				<p class="italic-text">Family name at birth</p>
				<div class="numeric-fields">
					<p>1.1a</p>
					<p class="field">${student.nume}</p>
				</div>
				<p>Inițiala (inițialele) prenumelui (prenumelor) tatălui/mamei</p>
				<p class="italic-text">Initial(s) of father's/mother's first name(s)</p>
				<div class="numeric-fields">
					<p>1.2a</p>
					<p class="field">${student.initialaTatalui}</p>
				</div>
				<p>Data nașterii (anul/luna/ziua)</p>
				<p class="italic-text">Date of birth (year/month/day)</p>
				<div class="numeric-fields">
					<p>1.3a</p>
					<p class="field-dob field-dob-first">${student.anulNasterii}</p>
					<p class="field-dob">${student.lunaNasterii}</p>
					<p class="field-dob">${student.ziuaNasterii}</p>
				</div>
			</div>
			<div class="column-half">
				<p>Nume de familie dupa căsătorie (dacă este cazul)</p>
				<p class="italic-text">Family name(s) (after marriage) (if applicable)</p>
				<div class="numeric-fields">
					<p>1.1b</p>
					<p class="field">${student.numeFamilieDupaCasatorie}</p>
				</div>
				<p>Prenumele</p>
				<p class="italic-text">First name(s)</p>
				<div class="numeric-fields">
					<p>1.1b</p>
					<p class="field">${student.prenume}</p>
				</div>
				<p>Locul nașterii (localitatea, județul/sectorul, țara)</p>
				<p class="italic-text">Place of birth</p>
				<div class="numeric-fields">
					<p>1.3b</p>
					<p class="field">${student.loculNasterii}</p>
				</div>
			</div>
		</div>
		<div class="display-flex last-row">
			<div class="column-small">
				<p>Numărul matricol</p>
				<p class="italic-text">Student enrolment number</p>
				<div class="numeric-fields">
					<p class="numbering">1.4</p>
					<p class="field no-margin">${student.numarMatricol}</p>
				</div>
			</div>
			<div class="column-large">
				<p>Codul numeric personal (CNP)</p>
				<p class="italic-text">Personal identification number</p>
				<div class="numeric-fields">
					<p class="field">${student.codNumericPersonal}</p>
				</div>
			</div>
			<div class="column-small">
				<p>Anul înmatriculării</p>
				<p class="italic-text">Year of enrollment</p>
				<div class="numeric-fields">
					<p>1.5</p>
					<p class="field">${student.anulInmatricularii}</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-2">
	<div class="container">
		<div class="text-align-center">
			<h4>2. INFORMAȚII PRIVIND CALIFICAREA</h4>
			<h4 class="italic-text">INFORMATION IDENTIFYING THE QUALIFICATION</h4>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Denumirea calificării și (dacă este cazul) titlul acordat (după promovarea examenului de finalizare a studiilor)</p>
			<p class="italic-text needs-padding">Name of qualification and (if applicable) title awarded (after passing the final examination)</p>
			<div class="numeric-fields all-space">
				<p class="numbering">2.1</p>
				<div class="field"><p>${department.denumireTitluCalificare}</p><p>${department.denumireTitluCalificareEngleza}</p></div>
			</div>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<p>Domeniul de studii</p>
				<p class="italic-text">Field of study</p>
				<div class="numeric-fields">
					<p class="numbering">2.2</p>
					<div class="field"><p>${department.nume}</p><p class="italic-text">${department.numeEngleza}</p></div>
				</div>
				<p>Numele și statutul instituției de învățământ superior care eliberează diploma (în limba română)</p>
				<p class="italic-text">Name and status of awarding institution</p>
				<div class="numeric-fields">
					<p>2.3a</p>
					<div class="field"><p>${department.numeInstitutie}</p><p class="italic-text">${department.numeInstitutieEngleza}</p></div>
				</div>
				<p>Numele și statutul instituției de învățământ superior care a asigurat școlarizarea (dacă diferă de 2.3a, în limba română)</p>
				<p class="italic-text">Name and status of institution administering studies <br/> (if different from 2.3a)
				</p>
				<div class="numeric-fields">
					<p>2.4a</p>
					<div class="field"><p>${department.numeInstitutieDifera}</p><p class="italic-text">${department.numeInstitutieDiferaEngleza}</p></div>
				</div>
			</div>
			<div class="column-half">
				<p>Programul de studii</p>
				<p class="italic-text">Programme of study</p>
				<div class="numeric-fields">
					<p>2.2b</p>
					<div class="field"><p>${department.programStudii}</p><p class="italic-text">${department.programStudiiEngleza}</p></div>
				</div>
				<p>Facultatea care organizează examenul de care eliberează diploma (în limba română) finalizare a studiilor</p>
				<p class="italic-text">Faculty administering the final examination</p>
				<div class="numeric-fields">
					<p>2.3b</p>
					<div class="field"><p>${department.facultate}</p><p class="italic-text">${department.facultateEngleza}</p></div>
				</div>
				<p>Facultatea care a asigurat școlarizarea <br/> (dacă diferă de 2.3b)</p>
				<p class="italic-text">Faculty administering studies <br/> (if different from 2.3b)</p>
				<div class="numeric-fields">
					<p>2.4b</p>
					<div class="field"><p>${department.facultateDifera}</p><p class="italic-text">${department.facultateDiferaEngleza}</p></div>
				</div>
			</div>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Limba (limbile) de studiu/examinare</p>
			<p class="italic-text needs-padding">Language(s) of instruction/examination</p>
			<div class="numeric-fields all-space">
				<p class="numbering">2.5</p>
				<div class="field"><p>${department.limbaStudiu}</p><p class="italic-text">${department.limbaStudiuEngleza}</p></div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-3">
	<div class="container">
		<div class="text-align-center">
			<h4>3. INFORMAȚII PRIVIND NIVELUL CALIFICĂRII</h4>
			<h4 class="italic-text">INFORMATION ON THE LEVEL OF THE QUALIFICATION</h4>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<p>Nivelul calificării</p><br/>
				<p class="italic-text">Level of qualification</p>
			</div>
			<div class="column-half">
				<p>Durata oficială a programului de studii și numărul de<br/>
					credite de studii transferabile (conform ECTS/SECT)</p>
				<p class="italic-text">Official length of the programme of study and<br/>
					number of ECTS/SECT credits</p>
			</div>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<p class="numbering">3.1</p>
					<div class="field">
						<p>${department.nivelCalificare}</p>
						<p class="italic-text">${department.nivelCalificareEngleza}</p>
					</div>
				</div>
			</div>
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<p>3.2</p>
					<div class="field">
						<p>${department.durataProgramului}</p>
						<p class="italic-text">${department.durataProgramuluiEngleza}</p>
					</div>
				</div>
			</div>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Condițiile de admitere</p>
			<p class="italic-text needs-padding">Access requirement(s)</p>
			<div class="numeric-fields all-space">
				<p class="numbering">3.3</p>
				<div class="field">
					<p>${department.conditiiAdmitere}</p>
					<p class="italic-text">${department.conditiiAdmitereEngleza}</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-4">
	<div class="container">
		<div class="text-align-center">
			<h4>4. INFORMAȚII PRIVIND CURRICULUMUL ȘI REZULTATELE OBȚINUTE</h4>
			<h4 class="italic-text">INFORMATION ON THE CURRICULUM AND RESULTS GAINED</h4>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Forma de învățământ</p>
			<p class="italic-text needs-padding">Mode of study</p>
			<div class="numeric-fields all-space">
				<p class="numbering">4.1</p>
				<div class="field">
					<p>${department.formaInvatamant}</p>
					<p class="italic-text">${department.formaInvatamantEngleza}</p>
				</div>
			</div>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Rezultatele învățării asigurate prin programul de studii</p>
			<p class="italic-text needs-padding">Learning outcomes of the study programme</p>
			<div class="numeric-fields all-space">
				<p class="numbering">4.2</p>
				<div class="field">
					<p class="justify-text">${department.rezultateleInvatarii}</p>
					<p class="italic-text">${department.rezultateleInvatariiEngleza}</p>
				</div>
			</div>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Detalii privind programul absolvit, calificativele/notele/creditele ECTS/SECT obținute (conform Registrului<br/>matricol al facultății, volumul nr. XII/2011)</p>
			<p class="italic-text needs-padding">Programme details and the individual grades / marks / ECTS/SECT credits obtained (according to Faculty Student<br/>Records, volume no. XII/2011)</p>
		</div>
		<div class="display-flex single-row">
			<div class="numeric-fields all-space">
				<p class="numbering">4.2</p>
				<div class="field-for-table">
					<div class="div-table">
						<div class="table-body">
							<div class="table-row table-header">
								<div class="table-cell col1-header">Nr.<br/>No</div>
								<div class="table-cell col2-header">Denumirea disciplinei<br/>Subject</div>
								<div class="table-cell col3-header divided-cell">
									<div class="my-border-bottom">
										<p><sup>3)</sup>Total ore<br/>Number of<br/> hours</p>
									</div>
									<div class="display-flex">
										<div class="col3 nobold number-of-hours1">C</div>
										<div class="col4 nobold number-of-hours2">S, LP, P</div>
									</div>
								</div>
								<div class="table-cell col4-header divided-cell">
									<div class="my-border-bottom">
										<p>Nota/Grade</p>
									</div>
									<div class="display-flex">
										<div class="col3 nobold semester-split">
											<p>Sem I<br/>1<sup>st</sup>sem</p>
										</div>
										<div class="col4 nobold semester-split2">
											<p>Sem II<br/>2<sup>nd</sup>sem</p>
										</div>
									</div>
								</div>
								<div class="table-cell col5-header divided-cell">
									<div class="my-border-bottom">
										<p>Nr. credite<br/>Number of<br/> ECTS/SECT credits</p>
									</div>
									<div class="display-flex">
										<div class="col3 nobold semester-split">
											<p>Sem I<br/>1<sup>st</sup>sem</p>
										</div>
										<div class="col4 nobold semester-split2">
											<p>Sem II<br/>2<sup>nd</sup>sem</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-year">
						<p>Anul I (anul universitar: ${year1})</p>
						<p>1st year of study (${year1} academic year)</p>
					</div>
					<div class="div-table">
						<div class="table-body">
							<x:forEach items="${enrollmentsYear1}" var="enrollments" varStatus="status">
								<div class="table-row">
									<div class="table-cell col1">${status.index + 1}</div>
									<div class="table-cell col2"><p>${enrollments.nume}</p><p class="italic-subject">${enrollments.numeEngleza}</p></div>
									<div class="table-cell col3">${enrollments.oreC}</div>
									<div class="table-cell col4">${enrollments.oreSLPP}</div>
									<div class="table-cell col5">${enrollments.notaSem1}</div>
									<div class="table-cell col6">${enrollments.notaSem2}</div>
									<div class="table-cell col7">${enrollments.crediteSem1}</div>
									<div class="table-cell col8">${enrollments.crediteSem2}</div>
								</div>
							</x:forEach>
						</div>
					</div>
					<div class="table-media myborder-bottom">
						<div class="column-half">
							<p>Promovat cu media<sup>4)</sup>: <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${yearGrade1}" /></p>
							<p>Pass, average grade per academic year:</p>
						</div>
						<div class="column-half">
							<p class="vertical-align">Total credite/Total ECTS/SECT credits: ${creditsYear1}</p>
						</div>
					</div>
					<div class="table-year">
						<p>Anul II (anul universitar: ${year2})</p>
						<p>2nd year of study (${year2} academic year)</p>
					</div>
					<div class="div-table">
						<div class="table-body">
							<x:forEach items="${enrollmentsYear2}" var="enrollments" varStatus="status">
								<div class="table-row">
									<div class="table-cell col1">${status.index + 1}</div>
									<div class="table-cell col2"><p>${enrollments.nume}</p><p class="italic-subject">${enrollments.numeEngleza}</p></div>
									<div class="table-cell col3">${enrollments.oreC}</div>
									<div class="table-cell col4">${enrollments.oreSLPP}</div>
									<div class="table-cell col5">${enrollments.notaSem1}</div>
									<div class="table-cell col6">${enrollments.notaSem2}</div>
									<div class="table-cell col7">${enrollments.crediteSem1}</div>
									<div class="table-cell col8">${enrollments.crediteSem2}</div>
								</div>
							</x:forEach>
						</div>
					</div>
					<div class="table-media myborder-bottom">
						<div class="column-half">
							<p>Promovat cu media<sup>4)</sup>: <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${yearGrade2}" /></p>
							<p>Pass, average grade per academic year:</p>
						</div>
						<div class="column-half">
							<p class="vertical-align">Total credite/Total ECTS/SECT credits: ${creditsYear2}</p>
						</div>
					</div>
					<div class="table-year">
						<p>Anul III (anul universitar: ${year3})</p>
						<p>3rd year of study (${year3} academic year)</p>
					</div>
					<div class="div-table">
						<div class="table-body">
							<x:forEach items="${enrollmentsYear3}" var="enrollments" varStatus="status">
								<div class="table-row">
									<div class="table-cell col1">${status.index + 1}</div>
									<div class="table-cell col2"><p>${enrollments.nume}</p><p class="italic-subject">${enrollments.numeEngleza}</p></div>
									<div class="table-cell col3">${enrollments.oreC}</div>
									<div class="table-cell col4">${enrollments.oreSLPP}</div>
									<div class="table-cell col5">${enrollments.notaSem1}</div>
									<div class="table-cell col6">${enrollments.notaSem2}</div>
									<div class="table-cell col7">${enrollments.crediteSem1}</div>
									<div class="table-cell col8">${enrollments.crediteSem2}</div>
								</div>
							</x:forEach>
						</div>
					</div>
					<div class="table-media myborder-bottom">
						<div class="column-half">
							<p>Promovat cu media<sup>4)</sup>: <fmt:formatNumber type = "number" maxFractionDigits = "2" value = "${yearGrade3}" /></p>
							<p>Pass, average grade per academic year:</p>
						</div>
						<div class="column-half">
							<p class="vertical-align">Total credite/Total ECTS/SECT credits: ${creditsYear3}</p>
						</div>
					</div>
					<div class="table-media my-border-bottom">
						<div class="three-cols border-totheright">
							<p>&nbsp;</p>
							<p>Promovat: DA</p>
							<p>&nbsp;</p>
							<p>Pass: YES</p>
							<p>&nbsp;</p>
						</div>
						<div class="three-cols border-totheright">
							<p>Media<sup>5)</sup> de promovare a studiilor (ponderat cu puncte de credit – dacă este cazul): <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${medie}" /></p>
							<p>Overall average grade (credit-weighted average – if available): <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${medie}" /></p>
						</div>
						<div class="three-cols">
							<p>&nbsp;</p>
							<p>Total credite: ${totalCredits}</p>
							<p>&nbsp;</p>
							<p>Total ECTS/SECT credits: ${totalCredits}</p>
							<p>&nbsp;</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-5">
	<div class="container">
		<div class="display-flex single-row">
			<div class="numeric-fields all-space">
				<p class="numbering">4.4</p>
				<div>
					<p>Sistemul de notare și, dacă sunt disponibile, informații privind distribuția la statistică a notelor</p>
					<p class="italic-text">Grading scheme and, if available, grade distribution guidance</p>
				</div>
			</div>
		</div>
		<div class="display-flex single-row">
			<div class="numeric-fields all-space needs-padding">
				<div class="field">
					<p>Notarea unei discipline se face pe o scală de la 1 la 10; notele acordate sunt numere întregi; nota minimă de promovare este 5, iar nota maximă este 10. Media minimă de promovare a anilor de studii pentru promoția ${student.anulAbsolvirii} domeniul de studii ${department.nume}, programul de studii ${department.programStudii} este <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${lowestAverage}" />, iar media maximă este <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${highestAverage}" />, titularul fiind clasat pe locul ${rank} dintr-un total de ${numar} absolvenți.</p>
					<p class="italic-text">Grades are integer numbers and given on a scale from 1 (the lowest grade) to 10 (the highest grade); the lowest  passing grade is 5. The passing overall average grades for class of ${student.anulAbsolvirii}, field of study ${department.numeEngleza}, study programme in ${department.programStudiiEngleza}, are: lowest average: <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${lowestAverage}" /> (out of 10) and highest average <fmt:formatNumber type = "number" minFractionDigits="2" maxFractionDigits = "2" value = "${highestAverage}" /> (out of 10). The degree holder is ranked ${rank} out of ${numar} graduates.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-5">
	<div class="container">
		<div class="text-align-center">
			<h4>5. INFORMAȚII SUPLIMENTARE</h4>
			<h4 class="italic-text">ADDITIONAL INFORMATION</h4>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<p>Informații suplimentare</p>
				<p class="italic-text">Additional information</p>
			</div>
			<div class="column-half">
				<p>Alte surse pentru obținerea mai multor informații</p>
				<p class="italic-text">Further information sources</p>
			</div>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<p class="numbering">5.1</p>
					<div class="field">
						<p>${diploma.informatiiSuplimentare}</p>
					</div>
				</div>
			</div>
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<p>5.2</p>
					<div class="field">
						<p>${faculty.sursaInformatii}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-6">
	<div class="container">
		<div class="text-align-center">
			<h4>6. INFORMAȚII PRIVIND DREPTURILE CONFERITE DE CALIFICARE ȘI DE TITLU (dacă este cazul)</h4>
			<h4 class="italic-text">INFORMATION ON THE FUNCTION OF THE QUALIFICATION AND DEGREE (if applicable)</h4>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Posibilități de continuare a studiilor (după promovarea examenului de finalizare)</p>
			<p class="italic-text needs-padding">Access to further study (after passing the final examination)</p>
			<div class="numeric-fields all-space">
				<p class="numbering">6.1</p>
				<div class="field">
					<p>${department.posibilitateStudii}</p>
					<p class="italic-text">${department.posibilitateStudiiEngleza}</p>
				</div>

			</div>
		</div>
		<div class="display-flex single-row">
			<p class="needs-padding">Statutul profesional</p>
			<p class="italic-text needs-padding">Professional status</p>
			<div class="numeric-fields all-space">
				<p class="numbering">6.2</p>
				<div class="field">
					<p class="justify-text">${department.statutProfesional}</p>
					<p class="italic-text">${department.statutProfesionalEngleza}</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-7">
	<div class="container">
		<div class="text-align-center">
			<h4>7. LEGALITATEA SUPLIMENTULUI</h4>
			<h4 class="italic-text">CERTIFICATION OF THE SUPPLEMENT</h4>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<div class="display-flex">
					<p class="column-half function">Funcția</p>
					<p class="italic-text column-half remove-padding">Position</p>
				</div>
				<div class="display-flex">
					<p class="column-half function">Funcția</p>
					<p class="italic-text column-half remove-padding">Position</p>
				</div>
			</div>
			<div class="column-half">
				<div class="display-flex">
					<p class="column-half function">Funcția</p>
					<p class="italic-text column-half remove-padding">Position</p>
				</div>
				<div class="display-flex">
					<p class="column-half function">Funcția</p>
					<p class="italic-text column-half remove-padding">Position</p>
				</div>
			</div>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<div>
						<p class="numbering">7.1</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p class="numbering">7.3</p>
					</div>
					<div class="field">
						<div class="display-flex myborder-bottom">
							<div class="column-half border-totheright">
								<p>Rector</p>
								<p>Rector</p>
								<p>${faculty.rector}</p>
							</div>
							<p class="italic-text column-half remove-padding"></p>
						</div>
						<div class="display-flex">
							<div class="column-half border-totheright">
								<p>Decan</p>
								<p>Dean</p>
								<p>${faculty.decan}</p>
							</div>
							<p class="italic-text column-half remove-padding"></p>
						</div>
					</div>
				</div>
			</div>
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<div>
						<p class="numbering">7.2</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p class="numbering">7.4</p>
					</div>
					<div class="field">
						<div class="display-flex myborder-bottom">
							<div class="column-half border-totheright">
								<p>Secretar șef universitate</p>
								<p>University Registrar</p>
								<p>${faculty.secretarUniversitate}</p>
							</div>
							<p class="italic-text column-half remove-padding"></p>
						</div>
						<div class="display-flex">
							<div class="column-half border-totheright">
								<p>Secretar șef facultate</p>
								<p>Faculty Registrar</p>
								<p>${faculty.secretarFacultate}</p>
							</div>
							<p class="italic-text column-half remove-padding"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p>&nbsp;</p>
		<div class="display-flex">
			<div class="column-half">
				<p><sup>6)</sup>Nr. și data eliberării</p>
				<p class="italic-text">No., dated</p>
			</div>
			<div class="column-half">
				<p>Ștampila sau sigiliul oficial</p>
				<p class="italic-text">Official stamp or seal</p>
			</div>
		</div>
		<div class="display-flex">
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<div>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p class="numbering">7.5</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
					<div class="field">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Acest document conține un număr de ${diploma.numarPagini} pagini.</p>
						<p>This document consists of ${diploma.numarPagini} pages.</p>
						<p>&nbsp;</p>
						<p>${diploma.dataEliberarii}</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
				</div>
			</div>
			<div class="column-half">
				<div class="numeric-fields more-text-possible">
					<div>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p class="numbering">7.6</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
					<div class="field">

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="data-section data-section-8">
	<div class="container">
		<div class="text-align-center">
			<h4>8. INFORMAȚII PRIVIND SISTEMUL NAȚIONAL DE ÎNVĂȚĂMÂNT</h4>
			<h4 class="italic-text">INFORMATION ON THE NATIONAL EDUCATION SYSTEM</h4>
			<h4>&nbsp;</h4>
		</div>
		<div class="photo-container">
			<img src="<x:url value="/resources/photo.jpg"/>" alt="that-photo" class="professional-training" />
		</div>
		<p>&nbsp;</p>
		<p>PREZENTARE GENERALĂ A SISTEMULUI NAȚIONAL DE ÎNVĂȚĂMÂNT SUPERIOR</p>
		<p class="italic-text">Overview of the national higher education system</p>
		<p>&nbsp;</p>
		<p>Accesul în învățământul superior se bazează pe diploma de bacalaureat (obținută la sfârșitul învățământului secundar superior), iar accesul la programe de master se bazează pe diploma obținută după finalizarea studiilor de licență (BA/BSc/BEng).</p>
		<p class="italic-text">Access to higher education is based on the baccalaureate diploma (obtained at the end of upper secondary education) and access to master programmes is based on the bachelor degree (BA/BSc/BEng).</p>
		<p>&nbsp;</p>
		<p>Studiile universitare de licență (BA/BSc/BEng) presupun 180-240 de puncte de credit, calculate în conformitate cu sistemul european de credite transferabile (ECTS/SECT) și se finalizează prin nivelul 6 din cadrul european al calificărilor pentru învățare pe tot parcursul vieții (EQF/CEC).</p>
		<p class="italic-text">Bachelor studies (BA/BSc/BEng) presuppose 180-240 credit points, calculated in accordance with the European Credit Transfer System (ECTS/SECT), and ends with the level 6 from the European Qualifications Framework for lifelong learning (EQF/CEC).</p>
		<p>&nbsp;</p>
		<p>Studiile universitare de master (MA/MSc/MEng) presupun 60-120 puncte de credit, calculate în conformitate cu sistemul european de credite transferbile (ECTS/SECT) și se finalizează prin nivelul 7 din EQF/CEC.</p>
		<p class="italic-text">Master studies (MA/MSc/MEng) presuppose 60-120 credit points, calculated in accordance with the European Credit Transfer System (ECTS/SECT), and ends with the level 7 EQF/CEC.</p>
		<p>&nbsp;</p>
		<p>Pentru profesii reglementate prin norme, recomandări sau bune practici europene, studii universitare de licență și masterat pot fi oferite comasat, într-un program unitar de studii universitare cu o durată cuprinsă între 5 și 6 ani, la învățământul cu frecvență, diplomele obținute fiind echivalente diplomei de master (în următoarele domenii de studiu: Medicină – 360 de ECTS/SECT, Stomatologie – 360 de ECTS/SECT, Farmacie – 300 ECTS/SECT, Medicină Veterinară – 360 ECTS/SECT, Arhitectură – 360 ECTS/SECT).</p>
		<p class="italic-text">For professions regulated by European norms, regulations or good practices, bachelor (BA/BSc/BEng) and master studies (MA/MSc/MEng) can be provided as part of a 5 to 6 year full-time programme of study, thus diplomas are recognised as master’s degree certificates (the following fields of study are considered: Medicine – 360 ECTS/SECT, Dentistry – 360 ECTS/SECT, Pharmacy – 300 ECTS/SECT, Veterinary Medicine – 360 ECTS/SECT, Architecture – 360 ECTS/SECT).</p>
		<p>&nbsp;</p>
		<p>Studiile universitare de doctorat conduc la o teză de doctorat, iar candidații care finalizează primesc diploma de doctor. Studiile universitare de doctorat permit dobândirea unei calificări de nivelul 8 din EQF/CEC.</p>
		<p class="italic-text">PhD studies result in a doctoral research thesis, while successful candidates are awarded a PhD diploma. Doctoral studies allow obtaining a qualification at level 8 EQF/CEC.</p>
		<p>&nbsp;</p>
		<p>Sistemul de învățământ superior românesc este un sistem deschis. Toate universitățile din România folosesc Sistemul Europeann de Credite Transferabile (ECTS/SECT).</p>
		<p class="italic-text">The Romanian higher education system is an open system. All Romanian universities use the European Credit Transfer System (ECTS/SECT).</p>
		<p>&nbsp;</p>
		<p>Programele de studii universitare pot fi organizate, după caz, conform reglementărilor legale în vigoare, la următoarele forme de învățământ: cu frecvență, cu frecvență redusă și la distanță.</p>
		<p class="italic-text">University programs can be organized, as appropriate, according to legal regulations, at the following forms of education: full time, part time and distantly.</p>
		<p>&nbsp;</p>
		<p>De asemenea, universitățile oferă programe de formare profesională continuă, pe baza cererilor de pe piața muncii.</p>
		<p class="italic-text">Universities also provide continuing professional training programmes based on the market demands.</p>
	</div>
	<div class="container special-container">
		<p>&nbsp;</p>
		<p>(*) În conformitate cu Legea nr. 1/2011</p>
		<p class="italic-text">According to Law no. 1/2011</p>
	</div>
</section>
<%@ include file="common/footer.jspf" %>