package ro.emanuel.dsgenerator.dao;

import java.util.List;

import ro.emanuel.dsgenerator.model.*;

public interface StudentDAO {

	void saveIntoGeneralList(Student student);

	void duplicateStudent(Student s);

	int getLastAddedStudentId();

	void addEnrollments(int idStudentDuplicated, int newStudent);

	void createUser(String username, String password, String authority);

	void addSubject(String name, String englishName);

	int getDepartmentId(String nume);

	List<AcademicYear> retrieveAcademicYears(int idStudent);

	void addEnrolment(Enrollments enrolment, int idStudent);

	void addAuthority(String username, String authority);

	void saveDiploma(Diploma diploma);
	
	void update(Student student);

	void updateEnrollment(Enrollments enrollments);

	void updateDepartment(Department d);

	int getSubjectId(String nume);

	int getUserId (int numarMatricol);

	float getMedia(int idStudent);

	void updateMedia(int idStudent, float medie);

	float getSumGradesSem1(int idStudent, String anulUniversitar);

	float getCountSubjectsSem1(int idStudent, String anulUniversitar);

	float getSumGradesSem2(int idStudent, String anulUniversitar);

	float getCountSubjectsSem2(int idStudent, String anulUniversitar);

	float getGradePerYear(int idStudent, String anulUniversitar);

	int getCreditsSumSem1(int idStudent, String anulUniversitar);

	int getCreditsSumSem2(int idStudent, String anulUniversitar);

	int getTotalCredits(int idStudent, String anulUniversitar);

	int enrollmentStudentId(int enrollmentId);

	List<Subject> retrieveAllSubjects();

	List<Department> retrieveAllDepartments();

	void addDepartment(String name, String facultate);
	
	Student get (int id);

	Diploma getDiploma(int id);

	Department getDepartmentDetails(String domeniuStudii);

	Department getDepartmentDetails(int id);

	Faculty getFacultyDetails(int id);

	Enrollments getEnrollment (int id);

	List <Enrollments> getGradesYear (int id, String anUniversitar);

	int count(String specializare, int anulAbsolvirii);

	int retrieveRank(int idStudent, String specializare, int anulAbsolvirii);

	float getHighestAverage(String specializare, int anulAbsolvirii);

	float getLowestAverage(String specializare, int anulAbsolvirii);
	
	void delete(int id);

	void deleteEnrollmentsByStudentId(int id);

	void deleteEnrollments(int id);

	void deleteDepartment(int id);

	void deleteDiploma(int id);
	
	List<Student> list();

	List<Department> listDepartments();

	List<Enrollments> listEnrollments(int id);

	List<Student> groupListFirstYear(String specializare, int an);

	List<Student> groupListLastYear(String specializare, int an);

	List<Student> listOneRecord(String user);

	int checkIfDiplomaExists(int id);

	void newDiploma(int id);

	String getStudentFirstName(int studentId);

	String getStudentLastName(int studentId);

	int retrieveEnrolmentNumberById(int idStudent);

	void requestAccount(String name, String email);
}
