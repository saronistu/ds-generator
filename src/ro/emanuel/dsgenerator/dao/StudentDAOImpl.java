package ro.emanuel.dsgenerator.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import ro.emanuel.dsgenerator.model.*;

import static java.lang.Integer.parseInt;

public class StudentDAOImpl implements StudentDAO {

	private JdbcTemplate jdbcTemplate;
	
	public StudentDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveIntoGeneralList(Student s) {
		String sql = "INSERT INTO student (id_department, nume, username, prenume, domeniu_studii, medie) VALUES (?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, getDepartmentId(s.getDomeniuStudii()), s.getNume(), s.getUsername(), s.getPrenume(), s.getDomeniuStudii(), 0);
	}

	@Override
	public void duplicateStudent(Student s) {
		String sql = "INSERT INTO student (id_department, nume, prenume, domeniu_studii, medie) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, getDepartmentId(s.getDomeniuStudii()), s.getNume(), s.getPrenume(), s.getDomeniuStudii(), 0);
	}

	@Override
	public int getLastAddedStudentId() {
		String sql = "SELECT id_student FROM student ORDER BY id_student DESC LIMIT 1";
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public void addEnrollments(int idStudentDuplicated, int newStudent) {
		String sql = "INSERT INTO enrollments (id_student, anul_universitar, id_subject, ore_C, ore_S_LP_P, nota_sem_1, nota_sem_2, credite_sem_1, credite_sem_2) SELECT "+ newStudent + ", anul_universitar, id_subject, ore_C, ore_S_LP_P, nota_sem_1, nota_sem_2, credite_sem_1, credite_sem_2 FROM enrollments WHERE id_student = " + idStudentDuplicated;
		jdbcTemplate.update(sql);
	}

	@Override
	public int getSubjectId(String nume) {
		String sql = "SELECT id_subjects FROM subjects WHERE name = '" + nume + "'";
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public int getDepartmentId(String nume) {
		String sql = "SELECT id_department FROM department WHERE domeniu_studii = '" + nume + "'";
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public int getUserId(int numarMatricol) {
		String sql = "SELECT id_student FROM student WHERE numar_matricol=" + numarMatricol;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	// Get grades sum and subject count

	@Override
	public float getSumGradesSem1(int idStudent, String anulUniversitar) {
		String sql = "SELECT SUM(nota_sem_1) FROM enrollments WHERE anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getCountSubjectsSem1(int idStudent, String anulUniversitar) {
		String sql = "SELECT COUNT(nota_sem_1) FROM enrollments WHERE nota_sem_1 IS NOT NULL AND NOT nota_sem_1 = 'A' AND NOT nota_sem_1 = '' AND anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getSumGradesSem2(int idStudent, String anulUniversitar) {
		String sql = "SELECT SUM(nota_sem_2) FROM enrollments WHERE anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getCountSubjectsSem2(int idStudent, String anulUniversitar) {
		String sql = "SELECT COUNT(nota_sem_2) FROM enrollments WHERE nota_sem_2 IS NOT NULL AND NOT nota_sem_2 = 'A' AND NOT nota_sem_2 = '' AND anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getGradePerYear(int idStudent, String anulUniversitar) {
		float averageSem1 = getSumGradesSem1(idStudent, anulUniversitar)/getCountSubjectsSem1(idStudent, anulUniversitar);
		float averageSem2 = getSumGradesSem2(idStudent, anulUniversitar)/getCountSubjectsSem2(idStudent, anulUniversitar);
		return (averageSem1 + averageSem2)/2;
	}

	// Get credits sum

	@Override
	public int getCreditsSumSem1(int idStudent, String anulUniversitar) {
		String sql = "SELECT SUM(credite_sem_1) FROM enrollments WHERE anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public int getCreditsSumSem2(int idStudent, String anulUniversitar) {
		String sql = "SELECT SUM(credite_sem_2) FROM enrollments WHERE anul_universitar = '" + anulUniversitar + "' AND id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public int getTotalCredits(int idStudent, String anulUniversitar) {
		int creditsSem1 = getCreditsSumSem1(idStudent, anulUniversitar);
		int creditsSem2 = getCreditsSumSem2(idStudent, anulUniversitar);
//		int suma = creditsSem1 + creditsSem2;
		return creditsSem1 + creditsSem2;
	}

	@Override
	public int enrollmentStudentId(int enrollmentId) {
		String sql = "SELECT id_student FROM enrollments WHERE id_enrollment = " + enrollmentId;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getMedia(int idStudent) {
		String sql = "SELECT medie FROM student WHERE id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Float.class);
	}

	@Override
	public void updateMedia(int idStudent, float medie) {
		String sql = "UPDATE student SET medie=? WHERE id_student=?";
		jdbcTemplate.update(sql, medie, idStudent);
	}

	@Override
	public void createUser(String username, String password, String authority) {
		String sql = "INSERT INTO users (username, password) VALUES ('" + username + "', '" + password +"')";
		addAuthority(username, authority);
		jdbcTemplate.update(sql);
	}

	@Override
	public void addSubject(String name, String englishName) {
		String sql = "INSERT INTO subjects (name, name_english) VALUES ('" + name + "', '" + englishName +"')";
		jdbcTemplate.update(sql);
	}

	@Override
	public void addDepartment(String name, String facultate) {
		if (facultate.equals("Teologie")) {
			String sql = "INSERT INTO department (id_faculty, domeniu_studii) VALUES (1, '" + name + "')";
			jdbcTemplate.update(sql);
		} else if (facultate.equals("Management")) {
			String sql = "INSERT INTO department (id_faculty, domeniu_studii) VALUES (2, '" + name + "')";
			jdbcTemplate.update(sql);
		}
	}

	@Override
	public void addEnrolment(Enrollments e, int idStudent) {
		int idSubject = getSubjectId(e.getNume());
		String sql = "INSERT INTO enrollments (id_student, anul_universitar, id_subject, ore_C, ore_S_LP_P, nota_sem_1, nota_sem_2, credite_sem_1, credite_sem_2) " +
				"VALUES (" + idStudent + ", ?, " + idSubject + ", ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, e.getAnUniversitar(), e.getOreC(), e.getOreSLPP(), e.getNotaSem1(), e.getNotaSem2(), e.getCrediteSem1(), e.getCrediteSem2());
	}

	@Override
	public void addAuthority(String username, String authority) {
		String sql = "INSERT INTO authorities (username, authority) VALUES ('" + username + "', '" + authority + "')";
		jdbcTemplate.update(sql);
	}

	@Override
	public void saveDiploma(Diploma d) {
		String sql = "UPDATE diploma SET serie=?, numar=?, informatii_suplimentare=?, numar_pagini=?, data_eliberarii=? WHERE id_diploma=?";
		jdbcTemplate.update(sql, d.getSerie(), d.getNumar(), d.getInformatiiSuplimentare(), d.getNumarPagini(), d.getDataEliberarii(), d.getId());
	}

	@Override
	public void update(Student s) {
		String sql = "UPDATE student SET nume=?, nume_familie_dupa_casatorie=?, initiala_tatalui_mamei=?, prenume=?, anul_nasterii=?, luna_nasterii=?, ziua_nasterii=?, locul_nasterii=?, numar_matricol=?, cod_numeric_personal=?, anul_inmatricularii=?, anul_absolvirii=?, domeniu_studii=? WHERE id_student=?";
		jdbcTemplate.update(sql, s.getNume(), s.getNumeFamilieDupaCasatorie(), s.getInitialaTatalui(), s.getPrenume(), s.getAnulNasterii(), s.getLunaNasterii(), s.getZiuaNasterii(), s.getLoculNasterii(), s.getNumarMatricol(), s.getCodNumericPersonal(), s.getAnulInmatricularii(), s.getAnulAbsolvirii(), s.getDomeniuStudii(), s.getId());
	}

	@Override
	public void updateEnrollment(Enrollments e) {
		String sql = "UPDATE enrollments SET anul_universitar=?, ore_C=?, ore_S_LP_P=?, nota_sem_1=?, nota_sem_2=?, credite_sem_1=?, credite_sem_2=? WHERE id_enrollment=?";
		jdbcTemplate.update(sql, e.getAnUniversitar(), e.getOreC(), e.getOreSLPP(), e.getNotaSem1(), e.getNotaSem2(), e.getCrediteSem1(), e.getCrediteSem2(), e.getId());
	}

	@Override
	public void updateDepartment(Department d) {
		String sql = "UPDATE department SET domeniu_studii=?, domeniu_studii_engleza=?, denumire_titlu_calificare=?, denumire_titlu_calificare_engleza=?, program_studii=?, program_studii_engleza=?, nume_institutie=?, nume_institutie_engleza=?, facultate=?, facultate_engleza=?, nume_institutie_difera=?, nume_institutie_difera_engleza=?, facultate_difera=?, facultate_difera_engleza=?, limba_studiu=?, limba_studiu_engleza=?, nivel_calificare=?, nivel_calificare_engleza=?, durata_programului=?, durata_programului_engleza=?, conditii_admitere=?, conditii_admitere_engleza=?, forma_invatamant=?, forma_invatamant_engleza=?, rezultatele_invatarii=?, rezultatele_invatarii_engleza=?, posibilitati_studii=?, posibilitati_studii_engleza=?, statut_profesional=?, statut_profesional_engleza=? WHERE id_department=?";
		jdbcTemplate.update(sql, d.getNume(), d.getNumeEngleza(), d.getDenumireTitluCalificare(), d.getDenumireTitluCalificareEngleza(), d.getProgramStudii(), d.getProgramStudiiEngleza(), d.getNumeInstitutie(), d.getNumeInstitutieEngleza(), d.getFacultate(), d.getFacultateEngleza(), d.getNumeInstitutieDifera(), d.getNumeInstitutieDiferaEngleza(), d.getFacultateDifera(), d.getFacultateDiferaEngleza(), d.getLimbaStudiu(), d.getLimbaStudiuEngleza(), d.getNivelCalificare(), d.getNivelCalificareEngleza(), d.getDurataProgramului(), d.getDurataProgramuluiEngleza(), d.getConditiiAdmitere(), d.getConditiiAdmitereEngleza(), d.getFormaInvatamant(), d.getFormaInvatamantEngleza(), d.getRezultateleInvatarii(), d.getRezultateleInvatariiEngleza(), d.getPosibilitateStudii(), d.getPosibilitateStudiiEngleza(), d.getStatutProfesional(), d.getStatutProfesionalEngleza(), d.getId());
	}

	@Override
	public Student get(int id) {
		String sql = "SELECT * FROM student WHERE id_student=" + id;
		ResultSetExtractor<Student> extractor = rs -> {
			if (rs.next()) {
				String username = rs.getString("username");
				String nume = rs.getString("nume");
				String numeFamilieDupaCasatorie = rs.getString("nume_familie_dupa_casatorie");
				String initialaTataluiMamei = rs.getString("initiala_tatalui_mamei");
				String prenume = rs.getString("prenume");
				String anulNasterii = rs.getString("anul_nasterii");
				String lunaNasterii = rs.getString("luna_nasterii");
				String ziuaNasterii = rs.getString("ziua_nasterii");
				String loculNasterii = rs.getString("locul_nasterii");
				int numarMatricol = rs.getInt("numar_matricol");
				String codNumericPersonal = rs.getString("cod_numeric_personal");
				int anulInmatricularii = rs.getInt("anul_inmatricularii");
				int anulAbsolvirii = rs.getInt("anul_absolvirii");
				String domeniuStudii = rs.getString("domeniu_studii");
				return new Student(id, username, nume, numeFamilieDupaCasatorie, initialaTataluiMamei, prenume, anulNasterii, lunaNasterii, ziuaNasterii, loculNasterii, numarMatricol, codNumericPersonal, anulInmatricularii, anulAbsolvirii, domeniuStudii);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public Diploma getDiploma(int id) {
		String sql = "SELECT * FROM diploma WHERE id_student=" + id;
		ResultSetExtractor<Diploma> extractor = rs -> {
			if (rs.next()) {
				int idDiploma = rs.getInt("id_diploma");
				String serie = rs.getString("serie");
				String numar = rs.getString("numar");
				String informatiiSuplimentare = rs.getString("informatii_suplimentare");
				int numarPagini = rs.getInt("numar_pagini");
				String dataEliberarii = rs.getString("data_eliberarii");
				return new Diploma(idDiploma, serie, numar, informatiiSuplimentare, numarPagini, dataEliberarii);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public Department getDepartmentDetails(String domeniuStudii) {
		String sql = "SELECT * FROM department WHERE domeniu_studii = '" + domeniuStudii + "'";
		ResultSetExtractor<Department> extractor = rs -> {
			if (rs.next()) {
				String domeniuStudii1 = rs.getString("domeniu_studii");
				String domeniuStudiiEngleza = rs.getString("domeniu_studii_engleza");
				String denumireTitluCalificare = rs.getString("denumire_titlu_calificare");
				String denumireTitluCalificareEngleza = rs.getString("denumire_titlu_calificare_engleza");
				String programStudii = rs.getString("program_studii");
				String programStudiiEngleza = rs.getString("program_studii_engleza");
				String numeInstitutie = rs.getString("nume_institutie");
				String numeInstitutieEngleza = rs.getString("nume_institutie_engleza");
				String facultate = rs.getString("facultate");
				String facultateEngleza = rs.getString("facultate_engleza");
				String numeInstitutieDifera = rs.getString("nume_institutie_difera");
				String numeInstitutieDiferaEngleza = rs.getString("nume_institutie_difera_engleza");
				String facultateDifera = rs.getString("facultate_difera");
				String facultateDiferaEngleza = rs.getString("facultate_difera_engleza");
				String limbaStudiu = rs.getString("limba_studiu");
				String limbaStudiuEngleza = rs.getString("limba_studiu_engleza");
				String nivelCalificare = rs.getString("nivel_calificare");
				String nivelCalificareEngleza = rs.getString("nivel_calificare_engleza");
				String durataProgramului = rs.getString("durata_programului");
				String durataProgramuluiEngleza = rs.getString("durata_programului_engleza");
				String conditiiAdmitere = rs.getString("conditii_admitere");
				String conditiiAdmitereEngleza = rs.getString("conditii_admitere_engleza");
				String formaInvatamant = rs.getString("forma_invatamant");
				String formaInvatamantEngleza = rs.getString("forma_invatamant_engleza");
				String rezultateleInvatarii = rs.getString("rezultatele_invatarii");
				String rezultateleInvatariiEngleza = rs.getString("rezultatele_invatarii_engleza");
				String posibilitateStudii = rs.getString("posibilitati_studii");
				String posibilitateStudiiEngleza = rs.getString("posibilitati_studii_engleza");
				String statutProfesional = rs.getString("statut_profesional");
				String statutProfesionalEngleza = rs.getString("statut_profesional_engleza");
				return new Department(domeniuStudii1, domeniuStudiiEngleza, denumireTitluCalificare, denumireTitluCalificareEngleza, programStudii, programStudiiEngleza, numeInstitutie, numeInstitutieEngleza, facultate, facultateEngleza, numeInstitutieDifera, numeInstitutieDiferaEngleza, facultateDifera, facultateDiferaEngleza, limbaStudiu, limbaStudiuEngleza, nivelCalificare, nivelCalificareEngleza, durataProgramului, durataProgramuluiEngleza, conditiiAdmitere, conditiiAdmitereEngleza, formaInvatamant, formaInvatamantEngleza, rezultateleInvatarii, rezultateleInvatariiEngleza, posibilitateStudii, posibilitateStudiiEngleza, statutProfesional, statutProfesionalEngleza);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public Department getDepartmentDetails(int id) {
		String sql = "SELECT * FROM department WHERE id_department = " + id;
		ResultSetExtractor<Department> extractor = rs -> {
			if (rs.next()) {
				return getDepartment(rs, id);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public Faculty getFacultyDetails(int id) {
		String sql = "SELECT faculty.rector, faculty.decan, faculty.secretar_universitate, faculty.secretar_facultate, faculty.surse_informatii FROM department INNER JOIN faculty ON department.id_faculty = faculty.id INNER JOIN student ON department.id_department = student.id_department WHERE student.id_student =" + id;
		ResultSetExtractor<Faculty> extractor = rs -> {
			if (rs.next()) {
				String rector = rs.getString("rector");
				String decan = rs.getString("decan");
				String secretarUniversitate = rs.getString("secretar_universitate");
				String secretarFacultate = rs.getString("secretar_facultate");
				String surseInformatii = rs.getString("surse_informatii");
				return new Faculty(rector, decan, secretarUniversitate, secretarFacultate, surseInformatii);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public Enrollments getEnrollment(int id) {
		String sql = "SELECT id_enrollment, subjects.name, ore_C, ore_S_LP_P, nota_sem_1, nota_sem_2, credite_sem_1, credite_sem_2, anul_universitar FROM enrollments INNER JOIN subjects ON enrollments.id_subject = subjects.id_subjects WHERE id_enrollment=" + id;
//		ResultSetExtractor<Enrollments> extractor = new ResultSetExtractor<>() {
//			@Override
//			public Enrollments extractData(ResultSet rs) throws SQLException, DataAccessException {
		ResultSetExtractor<Enrollments> extractor = rs -> {
			if (rs.next()) {
				String nume = rs.getString("name");
				String oreC = rs.getString("ore_C");
				String oreSLPP = rs.getString("ore_S_LP_P");
				String notaSem1 = rs.getString("nota_sem_1");
				String notaSem2 = rs.getString("nota_sem_2");
				String crediteSem1 = rs.getString("credite_sem_1");
				String crediteSem2 = rs.getString("credite_sem_2");
				String anUniversitar = rs.getString("anul_universitar");
				return new Enrollments(id, nume, oreC, oreSLPP, notaSem1, notaSem2, crediteSem1, crediteSem2, anUniversitar);
			}
			return null;
		};
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public List<Enrollments> getGradesYear(int id, String anUniversitar) {
		String sql = "SELECT subjects.name, subjects.name_english, enrollments.anul_universitar, enrollments.ore_C, enrollments.ore_S_LP_P, enrollments.nota_sem_1, enrollments.nota_sem_2, enrollments.credite_sem_1, enrollments.credite_sem_2 FROM enrollments INNER JOIN subjects ON enrollments.id_subject = subjects.id_subjects WHERE id_student = " + id + " AND anul_universitar = '" + anUniversitar + "'";
		RowMapper<Enrollments> rowMapper = (rs, rowNum) -> {
			String nume = rs.getString("name");
			String numeEngleza = rs.getString("name_english");
			String anUniversitar1 = rs.getString("anul_universitar");
			String oreC = rs.getString("ore_C");
			String oreSLPP = rs.getString("ore_S_LP_P");
			String notaSem1 = rs.getString("nota_sem_1");
			String notaSem2 = rs.getString("nota_sem_2");
			String crediteSem1 = rs.getString("credite_sem_1");
			String crediteSem2 = rs.getString("credite_sem_2");
			return new Enrollments(id, nume, numeEngleza, anUniversitar1, oreC, oreSLPP, notaSem1, notaSem2, crediteSem1, crediteSem2);
		};
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public int count(String specializare, int anulAbsolvirii) {
		String sql = "SELECT DISTINCT COUNT(student.id_student) FROM student WHERE anul_absolvirii = " + anulAbsolvirii + " AND student.domeniu_studii = '" + specializare + "'";
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public int retrieveRank(int idStudent, String specializare, int anulAbsolvirii) {
		String sql = "SELECT rank FROM (SELECT @rank:=@rank+1 AS rank, id_student FROM student, (SELECT @rank := 0) r WHERE domeniu_studii = '" + specializare + "' AND anul_absolvirii = " + anulAbsolvirii + " ORDER BY medie DESC) t WHERE id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public float getHighestAverage(String specializare, int anulAbsolvirii) {
		String sql = "SELECT medie FROM student WHERE domeniu_studii = '" + specializare +  "' AND anul_absolvirii = " + anulAbsolvirii + " ORDER BY medie DESC LIMIT 1";
		return jdbcTemplate.queryForObject(sql, Float.class);
	}

	@Override
	public float getLowestAverage(String specializare, int anulAbsolvirii) {
		String sql = "SELECT medie FROM student WHERE domeniu_studii = '" + specializare +  "' AND anul_absolvirii = " + anulAbsolvirii + " ORDER BY medie ASC LIMIT 1";
		return jdbcTemplate.queryForObject(sql, Float.class);
	}

	@Override
	public void delete(int id) {
		String sql = ("DELETE FROM student WHERE id_student=" + id);
		jdbcTemplate.update(sql);
	}

	@Override
	public void deleteEnrollmentsByStudentId(int id) {
		String sql = ("DELETE FROM enrollments WHERE id_student = " + id);
		jdbcTemplate.update(sql);
	}

	@Override
	public void deleteEnrollments(int id) {
		String sql = ("DELETE FROM enrollments WHERE id_enrollment = " + id);
		jdbcTemplate.update(sql);
	}

	@Override
	public void deleteDepartment(int id) {
		String sql = ("DELETE FROM department WHERE id_department = " + id);
		jdbcTemplate.update(sql);
	}

	@Override
	public void deleteDiploma(int id) {
		String sql = ("DELETE FROM diploma WHERE id_student = " + id);
		jdbcTemplate.update(sql);
	}

	@Override
	public List<Student> list() {
		String sql = "SELECT * FROM student ORDER BY id_student DESC";
//		RowMapper<Student> rowMapper = new RowMapper<>() {
//			@Override
//			public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
//				return getStudent(rs);
//			}
//		};
		RowMapper<Student> rowMapper = (rs, rowNum) -> getStudent(rs);
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Department> listDepartments() {
		String sql = "SELECT * FROM department";
//		RowMapper<Department> rowMapper = new RowMapper<>() {
//			@Override
//			public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
//				return getDepartment(rs);
//			}
//		};
		RowMapper<Department> rowMapper = (rs, rowNum) -> getDepartment(rs);
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Enrollments> listEnrollments(int id) {
		String sql = "SELECT id_enrollment, subjects.name, ore_C, ore_S_LP_P, nota_sem_1, nota_sem_2, credite_sem_1, credite_sem_2, anul_universitar FROM enrollments INNER JOIN subjects ON enrollments.id_subject = subjects.id_subjects WHERE id_student = " + id + " ORDER BY id_enrollment";
		RowMapper<Enrollments> rowMapper = (rs, rowNum) -> {
			int id1 = rs.getInt("id_enrollment");
			String nume = rs.getString("name");
			String oreC = rs.getString("ore_C");
			String oreSLPP = rs.getString("ore_S_LP_P");
			String notaSem1 = rs.getString("nota_sem_1");
			String notaSem2 = rs.getString("nota_sem_2");
			String crediteSem1 = rs.getString("credite_sem_1");
			String crediteSem2 = rs.getString("credite_sem_2");
			String anUniversitar = rs.getString("anul_universitar");
			return new Enrollments(id1, nume, oreC, oreSLPP, notaSem1, notaSem2, crediteSem1, crediteSem2, anUniversitar);
		};
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Subject> retrieveAllSubjects() {
		String sql = "SELECT * FROM subjects ORDER BY name ASC";
		RowMapper<Subject> rowMapper = (rs, rowNum) -> {
			String nume = rs.getString("name");
			return new Subject(nume);
		};
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Department> retrieveAllDepartments() {
		String sql = "SELECT * FROM department";
		RowMapper<Department> rowMapper = (rs, rowNum) -> {
			String nume = rs.getString("domeniu_studii");
			return new Department(nume);
		};
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<AcademicYear> retrieveAcademicYears(int idStudent) {
		String sql = "SELECT DISTINCT anul_universitar FROM enrollments WHERE id_student = " + idStudent + " ORDER BY anul_universitar ASC";
		RowMapper<AcademicYear> rowMapper = (rs, rowNum) -> {
			String academicYear = rs.getString("anul_universitar");
			return new AcademicYear(academicYear);
		};
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Student> groupListFirstYear(String specializare, int an) {
		String sql = "SELECT * FROM student WHERE domeniu_studii = '" + specializare + "' AND anul_inmatricularii = " + an;
		// Lambda
		RowMapper<Student> rowMapper = (rs, rowNum) -> getStudent(rs);
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Student> groupListLastYear(String specializare, int an) {
		String sql = "SELECT * FROM student WHERE domeniu_studii = '" + specializare + "' AND anul_absolvirii = " + an;
		// Lambda
		RowMapper<Student> rowMapper = (rs, rowNum) -> getStudent(rs);
		return jdbcTemplate.query(sql, rowMapper);
	}

	private Student getStudent(ResultSet rs) throws SQLException {
		int id = rs.getInt("id_student");
		String username = rs.getString("username");
		String nume = rs.getString("nume");
		String numeFamilieDupaCasatorie = rs.getString("nume_familie_dupa_casatorie");
		String initialaTataluiMamei = rs.getString("initiala_tatalui_mamei");
		String prenume = rs.getString("prenume");
		String anulNasterii = rs.getString("anul_nasterii");
		String lunaNasterii = rs.getString("luna_nasterii");
		String ziuaNasterii = rs.getString("ziua_nasterii");
		String loculNasterii = rs.getString("locul_nasterii");
		int numarMatricol = rs.getInt("numar_matricol");
		String codNumericPersonal = rs.getString("cod_numeric_personal");
		int anulInmatricularii = rs.getInt("anul_inmatricularii");
		int anulAbsolvirii = rs.getInt("anul_absolvirii");
		String domeniuStudii = rs.getString("domeniu_studii");
		return new Student(id, username, nume, numeFamilieDupaCasatorie, initialaTataluiMamei, prenume, anulNasterii, lunaNasterii, ziuaNasterii, loculNasterii, numarMatricol, codNumericPersonal, anulInmatricularii, anulAbsolvirii, domeniuStudii);
	}

	private Department getDepartment(ResultSet rs) throws SQLException {
		int id = rs.getInt("id_department");
		return getDepartment(rs, id);
	}

	private Department getDepartment(ResultSet rs, int id) throws SQLException {
		String domeniuStudii = rs.getString("domeniu_studii");
		String domeniuStudiiEngleza = rs.getString("domeniu_studii_engleza");
		String denumireTitluCalificare = rs.getString("denumire_titlu_calificare");
		String denumireTitluCalificareEngleza = rs.getString("denumire_titlu_calificare_engleza");
		String programStudii = rs.getString("program_studii");
		String programStudiiEngleza = rs.getString("program_studii_engleza");
		String numeInstitutie = rs.getString("nume_institutie");
		String numeInstitutieEngleza = rs.getString("nume_institutie_engleza");
		String facultate = rs.getString("facultate");
		String facultateEngleza = rs.getString("facultate_engleza");
		String numeInstitutieDifera = rs.getString("nume_institutie_difera");
		String numeInstitutieDiferaEngleza = rs.getString("nume_institutie_difera_engleza");
		String facultateDifera = rs.getString("facultate_difera");
		String facultateDiferaEngleza = rs.getString("facultate_difera_engleza");
		String limbaStudiu = rs.getString("limba_studiu");
		String limbaStudiuEngleza = rs.getString("limba_studiu_engleza");
		String nivelCalificare = rs.getString("nivel_calificare");
		String nivelCalificareEngleza = rs.getString("nivel_calificare_engleza");
		String durataProgramului = rs.getString("durata_programului");
		String durataProgramuluiEngleza = rs.getString("durata_programului_engleza");
		String conditiiAdmitere = rs.getString("conditii_admitere");
		String conditiiAdmitereEngleza = rs.getString("conditii_admitere_engleza");
		String formaInvatamant = rs.getString("forma_invatamant");
		String formaInvatamantEngleza = rs.getString("forma_invatamant_engleza");
		String rezultateleInvatarii = rs.getString("rezultatele_invatarii");
		String rezultateleInvatariiEngleza = rs.getString("rezultatele_invatarii_engleza");
		String posibilitateStudii = rs.getString("posibilitati_studii");
		String posibilitateStudiiEngleza = rs.getString("posibilitati_studii_engleza");
		String statutProfesional = rs.getString("statut_profesional");
		String statutProfesionalEngleza = rs.getString("statut_profesional_engleza");
		return new Department(id, domeniuStudii, domeniuStudiiEngleza, denumireTitluCalificare, denumireTitluCalificareEngleza, programStudii, programStudiiEngleza, numeInstitutie, numeInstitutieEngleza, facultate, facultateEngleza, numeInstitutieDifera, numeInstitutieDiferaEngleza, facultateDifera, facultateDiferaEngleza, limbaStudiu, limbaStudiuEngleza, nivelCalificare, nivelCalificareEngleza, durataProgramului, durataProgramuluiEngleza, conditiiAdmitere, conditiiAdmitereEngleza, formaInvatamant, formaInvatamantEngleza, rezultateleInvatarii, rezultateleInvatariiEngleza, posibilitateStudii, posibilitateStudiiEngleza, statutProfesional, statutProfesionalEngleza);
	}

	@Override
	public List<Student> listOneRecord(String username) {
		String sql = "SELECT * FROM student WHERE username='" + username + "'";
		RowMapper<Student> rowMapper = (rs, rowNum) -> getStudent(rs);
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public int checkIfDiplomaExists(int id) {
		String sql = "SELECT COUNT(id_student) FROM diploma WHERE id_student = " + id;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public void newDiploma(int id) {
		String sql = "INSERT INTO diploma (id_student) VALUES (" + id + ")";
		jdbcTemplate.update(sql);
	}

	@Override
	public String getStudentFirstName(int studentId) {
		String sql = "SELECT nume FROM student WHERE id_student = " + studentId;
		return jdbcTemplate.queryForObject(sql, String.class);
	}

	@Override
	public String getStudentLastName(int studentId) {
		String sql = "SELECT prenume FROM student WHERE id_student = " + studentId;
		return jdbcTemplate.queryForObject(sql, String.class);
	}

	@Override
	public int retrieveEnrolmentNumberById(int idStudent) {
		String sql = "SELECT numar_matricol FROM student WHERE id_student = " + idStudent;
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}

	@Override
	public void requestAccount(String name, String email) {
		String sql = "INSERT INTO `anonymous` (`name`, `email`) VALUES ('" + name + "', '" + email + "');";
		jdbcTemplate.update(sql);
	}
}
