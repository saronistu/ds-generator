package ro.emanuel.dsgenerator.login;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ro.emanuel.dsgenerator.dao.StudentDAO;
import ro.emanuel.dsgenerator.model.Anonymous;

@Controller
public class WelcomeController {

    private final StudentDAO studentDAO;

    public WelcomeController(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @RequestMapping (value = "/", method = RequestMethod.GET)
    public ModelAndView loginViewGet(ModelAndView model) {
//        model.addObject("name", retrieveLoggedInUserName());
        Anonymous anonymous = new Anonymous();
        model.addObject("anonymous", anonymous);
        model.setViewName("index");
        return model;
    }

    @RequestMapping (value = "requestUser", method = RequestMethod.POST)
    public ModelAndView requestedUser(@ModelAttribute Anonymous anonymous) {
        String name = anonymous.getName();
        String email = anonymous.getEmail();
        studentDAO.requestAccount(name, email);
        return new ModelAndView("formSubmitted");
    }

    private String retrieveLoggedInUserName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        if (principal instanceof UserDetails)
            return ((UserDetails) principal).getUsername();
        return principal.toString();
    }
}
