package ro.emanuel.dsgenerator.model;

public class Diploma {
    private int id;
    private String serie;
    private String numar;
    private String informatiiSuplimentare;
    private int numarPagini;
    private String dataEliberarii;

    public Diploma(){
    }

    public Diploma(int id, String serie, String numar, String informatiiSuplimentare, int numarPagini, String dataEliberarii) {
        this.id = id;
        this.serie = serie;
        this.numar = numar;
        this.informatiiSuplimentare = informatiiSuplimentare;
        this.numarPagini = numarPagini;
        this.dataEliberarii = dataEliberarii;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumar() {
        return numar;
    }

    public void setNumar(String numar) {
        this.numar = numar;
    }

    public String getInformatiiSuplimentare() {
        return informatiiSuplimentare;
    }

    public void setInformatiiSuplimentare(String informatiiSuplimentare) {
        this.informatiiSuplimentare = informatiiSuplimentare;
    }

    public int getNumarPagini() {
        return numarPagini;
    }

    public void setNumarPagini(int numarPagini) {
        this.numarPagini = numarPagini;
    }

    public String getDataEliberarii() {
        return dataEliberarii;
    }

    public void setDataEliberarii(String dataEliberarii) {
        this.dataEliberarii = dataEliberarii;
    }
}
