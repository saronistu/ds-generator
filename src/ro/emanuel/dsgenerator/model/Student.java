package ro.emanuel.dsgenerator.model;

public class Student {
	private Integer id;
	private String username;
	private String nume;
	private String numeFamilieDupaCasatorie;
	private String initialaTatalui;
	private String prenume;
	private String anulNasterii;
	private String lunaNasterii;
	private String ziuaNasterii;
	private String loculNasterii;
	private int numarMatricol;
	private String codNumericPersonal;
	private int anulInmatricularii;
	private int anulAbsolvirii;
	private String domeniuStudii;

	public Student() {
	}

	public Student(Integer id, String username) {
		this.id = id;
		this.username = username;
	}

	public Student(Integer id, String username, String nume, String numeFamilieDupaCasatorie, String initialaTatalui, String prenume, String anulNasterii, String lunaNasterii, String ziuaNasterii, String loculNasterii, int numarMatricol, String codNumericPersonal, int anulInmatricularii, int anulAbsolvirii, String domeniuStudii) {
		this.id = id;
		this.username = username;
		this.nume = nume;
		this.numeFamilieDupaCasatorie = numeFamilieDupaCasatorie;
		this.initialaTatalui = initialaTatalui;
		this.prenume = prenume;
		this.anulNasterii = anulNasterii;
		this.lunaNasterii = lunaNasterii;
		this.ziuaNasterii = ziuaNasterii;
		this.loculNasterii = loculNasterii;
		this.numarMatricol = numarMatricol;
		this.codNumericPersonal = codNumericPersonal;
		this.anulInmatricularii = anulInmatricularii;
		this.anulAbsolvirii = anulAbsolvirii;
		this.domeniuStudii = domeniuStudii;
	}

	public Student(String username, String nume, String numeFamilieDupaCasatorie, String initialaTatalui, String prenume, String anulNasterii, String lunaNasterii, String ziuaNasterii, String loculNasterii, int numarMatricol, String codNumericPersonal, int anulInmatricularii, String domeniuStudii) {
		this.username = username;
		this.nume = nume;
		this.numeFamilieDupaCasatorie = numeFamilieDupaCasatorie;
		this.initialaTatalui = initialaTatalui;
		this.prenume = prenume;
		this.anulNasterii = anulNasterii;
		this.lunaNasterii = lunaNasterii;
		this.ziuaNasterii = ziuaNasterii;
		this.loculNasterii = loculNasterii;
		this.numarMatricol = numarMatricol;
		this.codNumericPersonal = codNumericPersonal;
		this.anulInmatricularii = anulInmatricularii;
		this.domeniuStudii = domeniuStudii;
	}

	public Student(String domeniuStudii) {
		this.domeniuStudii = domeniuStudii;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getNumeFamilieDupaCasatorie() {
		return numeFamilieDupaCasatorie;
	}

	public void setNumeFamilieDupaCasatorie(String numeFamilieDupaCasatorie) {
		this.numeFamilieDupaCasatorie = numeFamilieDupaCasatorie;
	}

	public String getInitialaTatalui() {
		return initialaTatalui;
	}

	public void setInitialaTatalui(String initialaTatalui) {
		this.initialaTatalui = initialaTatalui;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getAnulNasterii() {
		return anulNasterii;
	}

	public void setAnulNasterii(String anulNasterii) {
		this.anulNasterii = anulNasterii;
	}

	public String getLunaNasterii() {
		return lunaNasterii;
	}

	public void setLunaNasterii(String lunaNasterii) {
		this.lunaNasterii = lunaNasterii;
	}

	public String getZiuaNasterii() {
		return ziuaNasterii;
	}

	public void setZiuaNasterii(String ziuaNasterii) {
		this.ziuaNasterii = ziuaNasterii;
	}

	public String getLoculNasterii() {
		return loculNasterii;
	}

	public void setLoculNasterii(String loculNasterii) {
		this.loculNasterii = loculNasterii;
	}

	public int getNumarMatricol() {
		return numarMatricol;
	}

	public void setNumarMatricol(int numarMatricol) {
		this.numarMatricol = numarMatricol;
	}

	public String getCodNumericPersonal() {
		return codNumericPersonal;
	}

	public void setCodNumericPersonal(String codNumericPersonal) {
		this.codNumericPersonal = codNumericPersonal;
	}

	public int getAnulInmatricularii() {
		return anulInmatricularii;
	}

	public void setAnulInmatricularii(int anulInmatricularii) {
		this.anulInmatricularii = anulInmatricularii;
	}

	public int getAnulAbsolvirii() {
		return anulAbsolvirii;
	}

	public void setAnulAbsolvirii(int anulAbsolvirii) {
		this.anulAbsolvirii = anulAbsolvirii;
	}

	public String getDomeniuStudii() {
		return domeniuStudii;
	}

	public void setDomeniuStudii(String domeniuStudii) {
		this.domeniuStudii = domeniuStudii;
	}
}
