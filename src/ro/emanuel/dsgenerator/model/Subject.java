package ro.emanuel.dsgenerator.model;

public class Subject {
    private String name;
    private String englishName;

    public Subject() {
    }

    public Subject(String name, String englishName) {
        this.name = name;
        this.englishName = englishName;
    }

    public Subject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String toString() {
        return name;
    }
}
