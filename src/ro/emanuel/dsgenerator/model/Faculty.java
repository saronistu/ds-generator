package ro.emanuel.dsgenerator.model;

public class Faculty {
    private int id;
    private String nume;
    private String rector;
    private String decan;
    private String secretarUniversitate;
    private String secretarFacultate;
    private String sursaInformatii;

    public Faculty() {
    }

    public Faculty(int id, String nume, String rector, String decan, String secretarUniversitate, String secretarFacultate, String sursaInformatii) {
        this.id = id;
        this.nume = nume;
        this.rector = rector;
        this.decan = decan;
        this.secretarUniversitate = secretarUniversitate;
        this.secretarFacultate = secretarFacultate;
        this.sursaInformatii = sursaInformatii;
    }

    public Faculty(String rector, String decan, String secretarUniversitate, String secretarFacultate, String sursaInformatii) {
        this.rector = rector;
        this.decan = decan;
        this.secretarUniversitate = secretarUniversitate;
        this.secretarFacultate = secretarFacultate;
        this.sursaInformatii = sursaInformatii;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getRector() {
        return rector;
    }

    public void setRector(String rector) {
        this.rector = rector;
    }

    public String getDecan() {
        return decan;
    }

    public void setDecan(String decan) {
        this.decan = decan;
    }

    public String getSecretarUniversitate() {
        return secretarUniversitate;
    }

    public void setSecretarUniversitate(String secretarUniversitate) {
        this.secretarUniversitate = secretarUniversitate;
    }

    public String getSecretarFacultate() {
        return secretarFacultate;
    }

    public void setSecretarFacultate(String secretarFacultate) {
        this.secretarFacultate = secretarFacultate;
    }

    public String getSursaInformatii() {
        return sursaInformatii;
    }

    public void setSursaInformatii(String sursaInformatii) {
        this.sursaInformatii = sursaInformatii;
    }
}
