package ro.emanuel.dsgenerator.model;

public class Enrollments {
    private int id;
    private String numarMatricol;
    private String nume;
    private String numeEngleza;
    private String anUniversitar;
    private String oreC;
    private String oreSLPP;
    private String notaSem1;
    private String notaSem2;
    private String crediteSem1;
    private String crediteSem2;

    public Enrollments (){
    }

    public Enrollments(int id, String nume, String numeEngleza, String anUniversitar, String oreC, String oreSLPP, String notaSem1, String notaSem2, String crediteSem1, String crediteSem2) {
        this.id = id;
        this.nume = nume;
        this.numeEngleza = numeEngleza;
        this.anUniversitar = anUniversitar;
        this.oreC = oreC;
        this.oreSLPP = oreSLPP;
        this.notaSem1 = notaSem1;
        this.notaSem2 = notaSem2;
        this.crediteSem1 = crediteSem1;
        this.crediteSem2 = crediteSem2;
    }

    public Enrollments(int id, String nume, String oreC, String oreSLPP, String notaSem1, String notaSem2, String crediteSem1, String crediteSem2, String anUniversitar) {
        this.id = id;
        this.nume = nume;
        this.oreC = oreC;
        this.oreSLPP = oreSLPP;
        this.notaSem1 = notaSem1;
        this.notaSem2 = notaSem2;
        this.crediteSem1 = crediteSem1;
        this.crediteSem2 = crediteSem2;
        this.anUniversitar = anUniversitar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumarMatricol() {
        return numarMatricol;
    }

    public void setNumarMatricol(String numarMatricol) {
        this.numarMatricol = numarMatricol;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getNumeEngleza() {
        return numeEngleza;
    }

    public void setNumeEngleza(String numeEngleza) {
        this.numeEngleza = numeEngleza;
    }

    public String getAnUniversitar() {
        return anUniversitar;
    }

    public void setAnUniversitar(String anUniversitar) {
        this.anUniversitar = anUniversitar;
    }

    public String getOreC() {
        return oreC;
    }

    public void setOreC(String oreC) {
        this.oreC = oreC;
    }

    public String getOreSLPP() {
        return oreSLPP;
    }

    public void setOreSLPP(String oreSLPP) {
        this.oreSLPP = oreSLPP;
    }

    public String getNotaSem1() {
        return notaSem1;
    }

    public void setNotaSem1(String notaSem1) {
        this.notaSem1 = notaSem1;
    }

    public String getNotaSem2() {
        return notaSem2;
    }

    public void setNotaSem2(String notaSem2) {
        this.notaSem2 = notaSem2;
    }

    public String getCrediteSem1() {
        return crediteSem1;
    }

    public void setCrediteSem1(String crediteSem1) {
        this.crediteSem1 = crediteSem1;
    }

    public String getCrediteSem2() {
        return crediteSem2;
    }

    public void setCrediteSem2(String crediteSem2) {
        this.crediteSem2 = crediteSem2;
    }
}
