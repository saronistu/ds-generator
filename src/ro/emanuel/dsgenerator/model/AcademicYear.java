package ro.emanuel.dsgenerator.model;

public class AcademicYear {
    private String academicYear;

    public AcademicYear() {
    }

    public AcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String numeAnUniversitar() {
        return academicYear;
    }
}
