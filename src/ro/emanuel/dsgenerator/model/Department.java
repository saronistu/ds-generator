package ro.emanuel.dsgenerator.model;

public class Department {
    private int id;
    // nume = domeniu studii
    private String nume;
    private String numeEngleza;
    private int an;
    private String denumireTitluCalificare;
    private String denumireTitluCalificareEngleza;
    private String programStudii;
    private String programStudiiEngleza;
    private String numeInstitutie;
    private String numeInstitutieEngleza;
    private String facultate;
    private String facultateEngleza;
    private String numeInstitutieDifera;
    private String numeInstitutieDiferaEngleza;
    private String facultateDifera;
    private String facultateDiferaEngleza;
    private String limbaStudiu;
    private String limbaStudiuEngleza;
    private String nivelCalificare;
    private String nivelCalificareEngleza;
    private String durataProgramului;
    private String durataProgramuluiEngleza;
    private String conditiiAdmitere;
    private String conditiiAdmitereEngleza;
    private String formaInvatamant;
    private String formaInvatamantEngleza;
    private String rezultateleInvatarii;
    private String rezultateleInvatariiEngleza;
    private String posibilitateStudii;
    private String posibilitateStudiiEngleza;
    private String statutProfesional;
    private String statutProfesionalEngleza;

    public Department() {
    }

    public Department(String nume) {
        this.nume = nume;
    }

    public Department(String nume, String numeEngleza, String denumireTitluCalificare, String denumireTitluCalificareEngleza, String programStudii, String programStudiiEngleza, String numeInstitutie, String numeInstitutieEngleza, String facultate, String facultateEngleza, String numeInstitutieDifera, String numeInstitutieDiferaEngleza, String facultateDifera, String facultateDiferaEngleza, String limbaStudiu, String limbaStudiuEngleza, String nivelCalificare, String nivelCalificareEngleza, String durataProgramului, String durataProgramuluiEngleza, String conditiiAdmitere, String conditiiAdmitereEngleza, String formaInvatamant, String formaInvatamantEngleza, String rezultateleInvatarii, String rezultateleInvatariiEngleza, String posibilitateStudii, String posibilitateStudiiEngleza, String statutProfesional, String statutProfesionalEngleza) {
        this.nume = nume;
        this.numeEngleza = numeEngleza;
        this.denumireTitluCalificare = denumireTitluCalificare;
        this.denumireTitluCalificareEngleza = denumireTitluCalificareEngleza;
        this.programStudii = programStudii;
        this.programStudiiEngleza = programStudiiEngleza;
        this.numeInstitutie = numeInstitutie;
        this.numeInstitutieEngleza = numeInstitutieEngleza;
        this.facultate = facultate;
        this.facultateEngleza = facultateEngleza;
        this.numeInstitutieDifera = numeInstitutieDifera;
        this.numeInstitutieDiferaEngleza = numeInstitutieDiferaEngleza;
        this.facultateDifera = facultateDifera;
        this.facultateDiferaEngleza = facultateDiferaEngleza;
        this.limbaStudiu = limbaStudiu;
        this.limbaStudiuEngleza = limbaStudiuEngleza;
        this.nivelCalificare = nivelCalificare;
        this.nivelCalificareEngleza = nivelCalificareEngleza;
        this.durataProgramului = durataProgramului;
        this.durataProgramuluiEngleza = durataProgramuluiEngleza;
        this.conditiiAdmitere = conditiiAdmitere;
        this.conditiiAdmitereEngleza = conditiiAdmitereEngleza;
        this.formaInvatamant = formaInvatamant;
        this.formaInvatamantEngleza = formaInvatamantEngleza;
        this.rezultateleInvatarii = rezultateleInvatarii;
        this.rezultateleInvatariiEngleza = rezultateleInvatariiEngleza;
        this.posibilitateStudii = posibilitateStudii;
        this.posibilitateStudiiEngleza = posibilitateStudiiEngleza;
        this.statutProfesional = statutProfesional;
        this.statutProfesionalEngleza = statutProfesionalEngleza;
    }


    public Department(int id, String nume, String numeEngleza, String denumireTitluCalificare, String denumireTitluCalificareEngleza, String programStudii, String programStudiiEngleza, String numeInstitutie, String numeInstitutieEngleza, String facultate, String facultateEngleza, String numeInstitutieDifera, String numeInstitutieDiferaEngleza, String facultateDifera, String facultateDiferaEngleza, String limbaStudiu, String limbaStudiuEngleza, String nivelCalificare, String nivelCalificareEngleza, String durataProgramului, String durataProgramuluiEngleza, String conditiiAdmitere, String conditiiAdmitereEngleza, String formaInvatamant, String formaInvatamantEngleza, String rezultateleInvatarii, String rezultateleInvatariiEngleza, String posibilitateStudii, String posibilitateStudiiEngleza, String statutProfesional, String statutProfesionalEngleza) {
        this.id = id;
        this.nume = nume;
        this.numeEngleza = numeEngleza;
        this.denumireTitluCalificare = denumireTitluCalificare;
        this.denumireTitluCalificareEngleza = denumireTitluCalificareEngleza;
        this.programStudii = programStudii;
        this.programStudiiEngleza = programStudiiEngleza;
        this.numeInstitutie = numeInstitutie;
        this.numeInstitutieEngleza = numeInstitutieEngleza;
        this.facultate = facultate;
        this.facultateEngleza = facultateEngleza;
        this.numeInstitutieDifera = numeInstitutieDifera;
        this.numeInstitutieDiferaEngleza = numeInstitutieDiferaEngleza;
        this.facultateDifera = facultateDifera;
        this.facultateDiferaEngleza = facultateDiferaEngleza;
        this.limbaStudiu = limbaStudiu;
        this.limbaStudiuEngleza = limbaStudiuEngleza;
        this.nivelCalificare = nivelCalificare;
        this.nivelCalificareEngleza = nivelCalificareEngleza;
        this.durataProgramului = durataProgramului;
        this.durataProgramuluiEngleza = durataProgramuluiEngleza;
        this.conditiiAdmitere = conditiiAdmitere;
        this.conditiiAdmitereEngleza = conditiiAdmitereEngleza;
        this.formaInvatamant = formaInvatamant;
        this.formaInvatamantEngleza = formaInvatamantEngleza;
        this.rezultateleInvatarii = rezultateleInvatarii;
        this.rezultateleInvatariiEngleza = rezultateleInvatariiEngleza;
        this.posibilitateStudii = posibilitateStudii;
        this.posibilitateStudiiEngleza = posibilitateStudiiEngleza;
        this.statutProfesional = statutProfesional;
        this.statutProfesionalEngleza = statutProfesionalEngleza;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getNumeEngleza() {
        return numeEngleza;
    }

    public void setNumeEngleza(String numeEngleza) {
        this.numeEngleza = numeEngleza;
    }

    public int getAn() {
        return an;
    }

    public void setAn(int an) {
        this.an = an;
    }

    public String getDenumireTitluCalificare() {
        return denumireTitluCalificare;
    }

    public void setDenumireTitluCalificare(String denumireTitluCalificare) {
        this.denumireTitluCalificare = denumireTitluCalificare;
    }

    public String getDenumireTitluCalificareEngleza() {
        return denumireTitluCalificareEngleza;
    }

    public void setDenumireTitluCalificareEngleza(String denumireTitluCalificareEngleza) {
        this.denumireTitluCalificareEngleza = denumireTitluCalificareEngleza;
    }

    public String getProgramStudii() {
        return programStudii;
    }

    public void setProgramStudii(String programStudii) {
        this.programStudii = programStudii;
    }

    public String getProgramStudiiEngleza() {
        return programStudiiEngleza;
    }

    public void setProgramStudiiEngleza(String programStudiiEngleza) {
        this.programStudiiEngleza = programStudiiEngleza;
    }

    public String getNumeInstitutie() {
        return numeInstitutie;
    }

    public void setNumeInstitutie(String numeInstitutie) {
        this.numeInstitutie = numeInstitutie;
    }

    public String getNumeInstitutieEngleza() {
        return numeInstitutieEngleza;
    }

    public void setNumeInstitutieEngleza(String numeInstitutieEngleza) {
        this.numeInstitutieEngleza = numeInstitutieEngleza;
    }

    public String getFacultate() {
        return facultate;
    }

    public void setFacultate(String facultate) {
        this.facultate = facultate;
    }

    public String getFacultateEngleza() {
        return facultateEngleza;
    }

    public void setFacultateEngleza(String facultateEngleza) {
        this.facultateEngleza = facultateEngleza;
    }

    public String getNumeInstitutieDifera() {
        return numeInstitutieDifera;
    }

    public void setNumeInstitutieDifera(String numeInstitutieDifera) {
        this.numeInstitutieDifera = numeInstitutieDifera;
    }

    public String getNumeInstitutieDiferaEngleza() {
        return numeInstitutieDiferaEngleza;
    }

    public void setNumeInstitutieDiferaEngleza(String numeInstitutieDiferaEngleza) {
        this.numeInstitutieDiferaEngleza = numeInstitutieDiferaEngleza;
    }

    public String getFacultateDifera() {
        return facultateDifera;
    }

    public void setFacultateDifera(String facultateDifera) {
        this.facultateDifera = facultateDifera;
    }

    public String getFacultateDiferaEngleza() {
        return facultateDiferaEngleza;
    }

    public void setFacultateDiferaEngleza(String facultateDiferaEngleza) {
        this.facultateDiferaEngleza = facultateDiferaEngleza;
    }

    public String getLimbaStudiu() {
        return limbaStudiu;
    }

    public void setLimbaStudiu(String limbaStudiu) {
        this.limbaStudiu = limbaStudiu;
    }

    public String getLimbaStudiuEngleza() {
        return limbaStudiuEngleza;
    }

    public void setLimbaStudiuEngleza(String limbaStudiuEngleza) {
        this.limbaStudiuEngleza = limbaStudiuEngleza;
    }

    public String getNivelCalificare() {
        return nivelCalificare;
    }

    public void setNivelCalificare(String nivelCalificare) {
        this.nivelCalificare = nivelCalificare;
    }

    public String getNivelCalificareEngleza() {
        return nivelCalificareEngleza;
    }

    public void setNivelCalificareEngleza(String nivelCalificareEngleza) {
        this.nivelCalificareEngleza = nivelCalificareEngleza;
    }

    public String getDurataProgramului() {
        return durataProgramului;
    }

    public void setDurataProgramului(String durataProgramului) {
        this.durataProgramului = durataProgramului;
    }

    public String getDurataProgramuluiEngleza() {
        return durataProgramuluiEngleza;
    }

    public void setDurataProgramuluiEngleza(String durataProgramuluiEngleza) {
        this.durataProgramuluiEngleza = durataProgramuluiEngleza;
    }

    public String getConditiiAdmitere() {
        return conditiiAdmitere;
    }

    public void setConditiiAdmitere(String conditiiAdmitere) {
        this.conditiiAdmitere = conditiiAdmitere;
    }

    public String getConditiiAdmitereEngleza() {
        return conditiiAdmitereEngleza;
    }

    public void setConditiiAdmitereEngleza(String conditiiAdmitereEngleza) {
        this.conditiiAdmitereEngleza = conditiiAdmitereEngleza;
    }

    public String getFormaInvatamant() {
        return formaInvatamant;
    }

    public void setFormaInvatamant(String formaInvatamant) {
        this.formaInvatamant = formaInvatamant;
    }

    public String getFormaInvatamantEngleza() {
        return formaInvatamantEngleza;
    }

    public void setFormaInvatamantEngleza(String formaInvatamantEngleza) {
        this.formaInvatamantEngleza = formaInvatamantEngleza;
    }

    public String getRezultateleInvatarii() {
        return rezultateleInvatarii;
    }

    public void setRezultateleInvatarii(String rezultateleInvatarii) {
        this.rezultateleInvatarii = rezultateleInvatarii;
    }

    public String getRezultateleInvatariiEngleza() {
        return rezultateleInvatariiEngleza;
    }

    public void setRezultateleInvatariiEngleza(String rezultateleInvatariiEngleza) {
        this.rezultateleInvatariiEngleza = rezultateleInvatariiEngleza;
    }

    public String getPosibilitateStudii() {
        return posibilitateStudii;
    }

    public void setPosibilitateStudii(String posibilitateStudii) {
        this.posibilitateStudii = posibilitateStudii;
    }

    public String getPosibilitateStudiiEngleza() {
        return posibilitateStudiiEngleza;
    }

    public void setPosibilitateStudiiEngleza(String posibilitateStudiiEngleza) {
        this.posibilitateStudiiEngleza = posibilitateStudiiEngleza;
    }

    public String getStatutProfesional() {
        return statutProfesional;
    }

    public void setStatutProfesional(String statutProfesional) {
        this.statutProfesional = statutProfesional;
    }

    public String getStatutProfesionalEngleza() {
        return statutProfesionalEngleza;
    }

    public void setStatutProfesionalEngleza(String statutProfesionalEngleza) {
        this.statutProfesionalEngleza = statutProfesionalEngleza;
    }

    public String toString() {
        return nume;
    }
}
