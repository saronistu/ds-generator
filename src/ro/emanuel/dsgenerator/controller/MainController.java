package ro.emanuel.dsgenerator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.emanuel.dsgenerator.dao.MetadataDAO;
import ro.emanuel.dsgenerator.dao.StudentDAO;
import ro.emanuel.dsgenerator.model.*;


import static java.lang.Integer.*;


@Controller
public class MainController {

//	@Autowired
//	private MetadataDAO metadataDAO;
//
//	@Autowired
//	private StudentDAO studentDAO;
	
	private final MetadataDAO metadataDAO;

	private final StudentDAO studentDAO;

	public MainController(MetadataDAO metadataDAO, StudentDAO studentDAO) {
		this.metadataDAO = metadataDAO;
		this.studentDAO = studentDAO;
	}

	@RequestMapping(value = "/indexLogged", method = RequestMethod.GET)
	public ModelAndView welcome(ModelAndView model) {
//		model.addObject("name", retrieveLoggedInUserName());
		model.setViewName("indexLogged");
		return model;
	}

	@RequestMapping(value = "/student-list", method = RequestMethod.GET)
	public ModelAndView studentList(ModelAndView model) {
		List<Student> listStudent = studentDAO.list();
		model.addObject("listStudent", listStudent);
		model.setViewName("student-list");
		return model;
	}

	@RequestMapping(value = "/view-departments", method = RequestMethod.GET)
	public ModelAndView departmentsList(ModelAndView model) {
		List<Department> departmentsList = studentDAO.listDepartments();
		model.addObject("departmentsList", departmentsList);
		model.setViewName("departments-list");
		return model;
	}

	@RequestMapping(value = "/grades-list", method = RequestMethod.GET)
	public ModelAndView gradesList(ModelAndView model, HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		float medie = studentDAO.getMedia(id);
		String numeStudent = studentDAO.getStudentFirstName(id) + " " + studentDAO.getStudentLastName(id);
		List<Enrollments> listEnrollments = studentDAO.listEnrollments(id);
		model.addObject("enrollmentsList", listEnrollments);
		model.addObject("medie", medie);
		model.addObject("numeStudent", numeStudent);
		model.addObject("id", id);
		model.setViewName("grades-list");
		return model;
	}

	@RequestMapping(value = "/my-record", method = RequestMethod.GET)
	public ModelAndView getRecord(ModelAndView model) {
		List<Student> listStudent = studentDAO.listOneRecord(retrieveLoggedInUserName());
		model.addObject("listStudent", listStudent);
		model.setViewName("student-list");
		return model;
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView newStudent(ModelAndView model) {
		Student newStudent = new Student();
		List<Department> allDepartments = studentDAO.retrieveAllDepartments();
		ArrayList<String> departments = new ArrayList<>();
		for (Department department: allDepartments) {
			departments.add(department.toString());
		}
		model.addObject("allDepartments", departments);
		model.addObject("student", newStudent);
		model.setViewName("student_form_new");
		return model;
	}

	@RequestMapping(value = "/savenew", method = RequestMethod.POST)
	public ModelAndView saveNewStudent(@ModelAttribute Student student) {
		studentDAO.saveIntoGeneralList(student);
		return new ModelAndView("redirect:/student-list");
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "next")
	public ModelAndView toDiplomaInfo(@ModelAttribute Student student, HttpServletRequest request) {
		studentDAO.update(student);
		int id = parseInt(request.getParameter("id"));
		Diploma diploma = studentDAO.getDiploma(id);
		ModelAndView model = new ModelAndView("diploma-info");
		model.addObject("diploma", diploma);
		return model;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "save")
	public ModelAndView saveStudent(@ModelAttribute Student student) {
		studentDAO.update(student);
		return new ModelAndView("redirect:/student-list");
	}

	@RequestMapping(value = "/save-grade", method = RequestMethod.POST)
	public ModelAndView saveEnrollment(@ModelAttribute Enrollments enrollments) {
		studentDAO.updateEnrollment(enrollments);
		return new ModelAndView("redirect:grades-list?id=" + studentDAO.enrollmentStudentId(enrollments.getId()));
	}

	@RequestMapping(value = "/save-department", method = RequestMethod.POST)
	public ModelAndView saveDepartment(@ModelAttribute Department department) {
		studentDAO.updateDepartment(department);
		return new ModelAndView("redirect:/view-departments");
	}

	@RequestMapping(value = "/save-diploma", method = RequestMethod.POST)
	public ModelAndView saveDiploma(@ModelAttribute Diploma diploma) {
		studentDAO.saveDiploma(diploma);
		return new ModelAndView("redirect:/student-list");
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView editStudent(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		Student student = studentDAO.get(id);
		if (studentDAO.checkIfDiplomaExists(id) == 0) {
			studentDAO.newDiploma(id);
		}
		ModelAndView model = new ModelAndView("student_form");
		List<Department> allDepartments = studentDAO.retrieveAllDepartments();
		ArrayList<String> departments = new ArrayList<>();
		for (Department department: allDepartments) {
			departments.add(department.toString());
		}
		model.addObject("allDepartments", departments);
		model.addObject("student", student);
		return model;
	}

	@RequestMapping(value = "/edit-department", method = RequestMethod.GET)
	public ModelAndView editDepartment(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		Department department = studentDAO.getDepartmentDetails(id);
		ModelAndView model = new ModelAndView("department_form");
		model.addObject("department", department);
		return model;
	}

	@RequestMapping(value = "/edit-grade", method = RequestMethod.GET)
	public ModelAndView editGrade(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		Enrollments enrollments = studentDAO.getEnrollment(id);
		ModelAndView model = new ModelAndView("edit-grade");
		List<Subject> allSubjects = studentDAO.retrieveAllSubjects();
		ArrayList<String> subjects = new ArrayList<>();
		for (Subject subject: allSubjects) {
			subjects.add(subject.toString());
		}
		model.addObject("allSubjects", subjects);
		model.addObject("enrollments", enrollments);
		return model;
	}

	@RequestMapping(value = "/duplicate", method = RequestMethod.GET)
	public ModelAndView duplicateStudent(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		Student student = studentDAO.get(id);
		studentDAO.duplicateStudent(student);
		studentDAO.addEnrollments(id, studentDAO.getLastAddedStudentId());
		return new ModelAndView("redirect:/student-list");
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteStudent(@RequestParam Integer id) {
		if (studentDAO.checkIfDiplomaExists(id) != 0) {
			studentDAO.deleteDiploma(id);
		}
		studentDAO.deleteEnrollmentsByStudentId(id);
		studentDAO.delete(id);
		return new ModelAndView("redirect:/student-list");
	}

	@RequestMapping(value = "/delete-grade", method = RequestMethod.GET)
	public ModelAndView deleteEnrollement(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		int idStudent = studentDAO.enrollmentStudentId(id);
		studentDAO.deleteEnrollments(id);
		calculateMedia(idStudent);
		return new ModelAndView("redirect:grades-list?id=" + idStudent);
	}

	@RequestMapping(value = "/delete-department", method = RequestMethod.GET)
	public ModelAndView deleteDepartment(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		studentDAO.deleteDepartment(id);
		return new ModelAndView("redirect:view-departments");
	}

	@RequestMapping(value = "/create-user", method = RequestMethod.GET)
	public ModelAndView createUser(@ModelAttribute User user) {
		ModelAndView model = new ModelAndView("create-user");
		model.addObject("user", user);
		return model;
	}

	@RequestMapping(value = "/create-user", method = RequestMethod.POST)
	public ModelAndView createNewUser(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String authority = request.getParameter("authority");
		studentDAO.createUser(username, password, authority);
		return new ModelAndView("redirect:/student-list");
	}

	@RequestMapping(value = "/group-list", method = RequestMethod.GET)
	public ModelAndView listByGroup(@ModelAttribute Department department) {
		ModelAndView model = new ModelAndView("group-list");
		List<Department> allDepartments = studentDAO.retrieveAllDepartments();
		ArrayList<String> departments = new ArrayList<>();
		for (Department myDepartment: allDepartments) {
			departments.add(myDepartment.toString());
		}
		model.addObject("allDepartments", departments);
		model.addObject("department", department);
		return model;
	}

	@RequestMapping(value = "/group-list", method = RequestMethod.POST)
	public ModelAndView listGroup(HttpServletRequest request, ModelAndView model) {
		String specializare = request.getParameter("nume");
		int an = parseInt(request.getParameter("an"));
		List<Student> listStudent = studentDAO.groupListFirstYear(specializare, an);
		model.setViewName("student-list");
		model.addObject("listStudent", listStudent);
		return model;
	}

	@RequestMapping(value = "/group-list-grad", method = RequestMethod.POST)
	public ModelAndView listGroupGrad(HttpServletRequest request, ModelAndView model) {
		String specializare = request.getParameter("nume");
		int an = parseInt(request.getParameter("an"));
		List<Student> listStudent = studentDAO.groupListLastYear(specializare, an);
		model.setViewName("student-list");
		model.addObject("listStudent", listStudent);
		return model;
	}

	@RequestMapping(value = "/add-subject", method = RequestMethod.GET)
	public ModelAndView addSubject(@ModelAttribute Subject subject) {
		ModelAndView model = new ModelAndView("add-subject");
		model.addObject("subject", subject);
		return model;
	}

	@RequestMapping(value = "/add-subject", method = RequestMethod.POST)
	public ModelAndView addSubjectPost(HttpServletRequest request) {
		String name = request.getParameter("name");
		String englishName = request.getParameter("englishName");
		studentDAO.addSubject(name, englishName);
		return new ModelAndView("redirect:/add-subject");
	}

	@RequestMapping(value = "/add-departments", method = RequestMethod.GET)
	public ModelAndView addDepartments(@ModelAttribute Department department) {
		ModelAndView model = new ModelAndView("add-department");
		model.addObject("department", department);
		return model;
	}

	@RequestMapping(value = "/add-departments", method = RequestMethod.POST)
	public ModelAndView addDepartmentsPost(HttpServletRequest request) {
		String name = request.getParameter("nume");
		String facultate = request.getParameter("facultate");
		studentDAO.addDepartment(name, facultate);
		return new ModelAndView("redirect:/view-departments");
	}

	@RequestMapping(value = "/enrol-student", method = RequestMethod.GET)
	public ModelAndView enrolStudent(@ModelAttribute Enrollments enrol, @ModelAttribute Student student) {
		ModelAndView model = new ModelAndView("enrol-student");
		List<Subject> allSubjects = studentDAO.retrieveAllSubjects();
		ArrayList<String> subjects = new ArrayList<>();
		for (Subject subject: allSubjects) {
			subjects.add(subject.toString());
		}
		int numarMatricol = studentDAO.retrieveEnrolmentNumberById(student.getId());
		model.addObject("allSubjects", subjects);
		model.addObject("enrol", enrol);
		model.addObject("student", student);
		model.addObject("numarMatricol", numarMatricol);
		return model;
	}

	@RequestMapping(value = "/enrol-anyStudent", method = RequestMethod.GET)
	public ModelAndView enrolStudentRaw(@ModelAttribute Enrollments enrol) {
		ModelAndView model = new ModelAndView("enrol-anyStudent");
		List<Subject> allSubjects = studentDAO.retrieveAllSubjects();
		ArrayList<String> subjects = new ArrayList<>();
		for (Subject subject: allSubjects) {
			subjects.add(subject.toString());
		}
		model.addObject("allSubjects", subjects);
		model.addObject("enrol", enrol);
		return model;
	}

	@RequestMapping(value = "/enrol-student", method = RequestMethod.POST)
	public ModelAndView enrolStudent(HttpServletRequest request, @ModelAttribute Enrollments enrolment) {
		int idStudent = parseInt(request.getParameter("id"));
		if (request.getParameter("anUniversitar").length() != 9) {
			return new ModelAndView("error");
		}
		studentDAO.addEnrolment(enrolment, idStudent);
		calculateMedia(idStudent);
		return new ModelAndView("redirect:grades-list?id=" + idStudent);
	}

	@RequestMapping(value = "/enrol-anyStudent", method = RequestMethod.POST)
	public ModelAndView enrolAnyStudent(HttpServletRequest request, @ModelAttribute Enrollments enrolment) {
		int idStudent = parseInt(request.getParameter("id"));
		studentDAO.addEnrolment(enrolment, idStudent);
		calculateMedia(idStudent);
		return new ModelAndView("redirect:/enrol-anyStudent");
	}

	int totalCredits;
	String year1, year2, year3, year4;
	@RequestMapping(value = "/generate", method = RequestMethod.GET)
	public ModelAndView generateSupplement(HttpServletRequest request) {
		int id = parseInt(request.getParameter("id"));
		Student student = studentDAO.get(id);
		Metadata metadata = metadataDAO.get(1);
		Department department = studentDAO.getDepartmentDetails(student.getDomeniuStudii());
		Faculty faculty = studentDAO.getFacultyDetails(id);
		Diploma diploma = studentDAO.getDiploma(id);
		float medie = studentDAO.getMedia(id);
		float lowestAverage = studentDAO.getLowestAverage(student.getDomeniuStudii(), student.getAnulAbsolvirii());
		float highestAverage = studentDAO.getHighestAverage(student.getDomeniuStudii(), student.getAnulAbsolvirii());
		List<AcademicYear> academicYears = studentDAO.retrieveAcademicYears(id);
		int numarStudenti = studentDAO.count(student.getDomeniuStudii(), student.getAnulAbsolvirii());
		int rank = studentDAO.retrieveRank(id, student.getDomeniuStudii(), student.getAnulAbsolvirii());
		ModelAndView model = getSupplement(academicYears.size());
		switch (academicYears.size()) {
			case 1:
				forYearOne(id, academicYears, model);
				totalCredits = studentDAO.getTotalCredits(id, year1);
				model.addObject("totalCredits", totalCredits);
				break;
			case 2:
				forYearOne(id, academicYears, model);
				forYearTwo(id, academicYears, model);
				totalCredits = studentDAO.getTotalCredits(id, year1) + studentDAO.getTotalCredits(id, year2);
				model.addObject("totalCredits", totalCredits);
				break;
			case 3:
				forYearOne(id, academicYears, model);
				forYearTwo(id, academicYears, model);
				forYearThree(id, academicYears, model);
				totalCredits = studentDAO.getTotalCredits(id, year1) + studentDAO.getTotalCredits(id, year2) + studentDAO.getTotalCredits(id, year3);
				model.addObject("totalCredits", totalCredits);
				break;
			case 4:
				forYearOne(id, academicYears, model);
				forYearTwo(id, academicYears, model);
				forYearThree(id, academicYears, model);
				forYearFour(id, academicYears, model);
				totalCredits = studentDAO.getTotalCredits(id, year1) + studentDAO.getTotalCredits(id, year2) + studentDAO.getTotalCredits(id, year3) + studentDAO.getTotalCredits(id, year4);
				model.addObject("totalCredits", totalCredits);
				break;
		}
		model.addObject("academicYears", academicYears);
		model.addObject("student", student);
		model.addObject("department", department);
		model.addObject("faculty", faculty);
		model.addObject("diploma", diploma);
		model.addObject("metadata", metadata);
		model.addObject("rank", rank);
		model.addObject("numar", numarStudenti);
		model.addObject("medie", medie);
		model.addObject("lowestAverage", lowestAverage);
		model.addObject("highestAverage", highestAverage);
		return model;
	}

	private ModelAndView getSupplement(int academicYears) {
		switch (academicYears) {
			case 1:
				return new ModelAndView("supplement1");
			case 2:
				return new ModelAndView("supplement2");
			case 3:
				return new ModelAndView("supplement3");
			case 4:
				return new ModelAndView("supplement4");
			default:
				return new ModelAndView("supplement");
		}
	}

	private void calculateMedia(int idStudent) {
		float sum, average;
		List<AcademicYear> academicYears = studentDAO.retrieveAcademicYears(idStudent);
		switch (academicYears.size()) {
			case 1:
				year1 = getNthAcademicYear(academicYears, 1);
				average = studentDAO.getGradePerYear(idStudent, year1);
				studentDAO.updateMedia(idStudent, average);
				break;
			case 2:
				year1 = getNthAcademicYear(academicYears, 1);
				year2 = getNthAcademicYear(academicYears, 2);
				sum = studentDAO.getGradePerYear(idStudent, year1) + studentDAO.getGradePerYear(idStudent, year2);
				average = sum / 2;
				studentDAO.updateMedia(idStudent, average);
				break;
			case 3:
				year1 = getNthAcademicYear(academicYears, 1);
				year2 = getNthAcademicYear(academicYears, 2);
				year3 = getNthAcademicYear(academicYears, 3);
				sum = studentDAO.getGradePerYear(idStudent, year1) + studentDAO.getGradePerYear(idStudent, year2) + studentDAO.getGradePerYear(idStudent, year3);
				average = sum / 3;
				studentDAO.updateMedia(idStudent, average);
				break;
			case 4:
				year1 = getNthAcademicYear(academicYears, 1);
				year2 = getNthAcademicYear(academicYears, 2);
				year3 = getNthAcademicYear(academicYears, 3);
				year4 = getNthAcademicYear(academicYears, 4);
				sum = studentDAO.getGradePerYear(idStudent, year1) + studentDAO.getGradePerYear(idStudent, year2) + studentDAO.getGradePerYear(idStudent, year3) + studentDAO.getGradePerYear(idStudent, year4);
				average = sum / 4;
				studentDAO.updateMedia(idStudent, average);
				break;
		}
	}

	private void forYearFour(int id, List<AcademicYear> academicYears, ModelAndView model) {
		year4 = getNthAcademicYear(academicYears, 4);
		int creditsYear4 = studentDAO.getTotalCredits(id, year4);
		List<Enrollments> enrollmentsYear4 = studentDAO.getGradesYear(id, getNthAcademicYear(academicYears, 4));
		float yearGrade4 = studentDAO.getGradePerYear(id, year4);
		model.addObject("yearGrade4", yearGrade4);
		model.addObject("creditsYear4", creditsYear4);
		model.addObject("enrollmentsYear4", enrollmentsYear4);
		model.addObject("year4", year4);
	}

	private void forYearThree(int id, List<AcademicYear> academicYears, ModelAndView model) {
		year3 = getNthAcademicYear(academicYears, 3);
		int creditsYear3 = studentDAO.getTotalCredits(id, year3);
		List<Enrollments> enrollmentsYear3 = studentDAO.getGradesYear(id, getNthAcademicYear(academicYears, 3));
		float yearGrade3 = studentDAO.getGradePerYear(id, year3);
		model.addObject("yearGrade3", yearGrade3);
		model.addObject("creditsYear3", creditsYear3);
		model.addObject("enrollmentsYear3", enrollmentsYear3);
		model.addObject("year3", year3);
	}

	private void forYearTwo(int id, List<AcademicYear> academicYears, ModelAndView model) {
		year2 = getNthAcademicYear(academicYears, 2);
		int creditsYear2 = studentDAO.getTotalCredits(id, year2);
		List<Enrollments> enrollmentsYear2 = studentDAO.getGradesYear(id, getNthAcademicYear(academicYears, 2));
		float yearGrade2 = studentDAO.getGradePerYear(id, year2);
		model.addObject("yearGrade2", yearGrade2);
		model.addObject("creditsYear2", creditsYear2);
		model.addObject("enrollmentsYear2", enrollmentsYear2);
		model.addObject("year2", year2);
	}

	private void forYearOne(int id, List<AcademicYear> academicYears, ModelAndView model) {
		year1 = getNthAcademicYear(academicYears, 1);
		int creditsYear1 = studentDAO.getTotalCredits(id, year1);
		List<Enrollments> enrollmentsYear1 = studentDAO.getGradesYear(id, getNthAcademicYear(academicYears, 1));
		float yearGrade1 = studentDAO.getGradePerYear(id, year1);
		model.addObject("yearGrade1", yearGrade1);
		model.addObject("creditsYear1", creditsYear1);
		model.addObject("enrollmentsYear1", enrollmentsYear1);
		model.addObject("year1", year1);
	}

	public String getNthAcademicYear(List<AcademicYear> academicYears, int year) {
		ArrayList<String> anUniversitar = new ArrayList<>();
		for (AcademicYear ay: academicYears) {
			anUniversitar.add(ay.numeAnUniversitar());
		}
		return anUniversitar.get(year-1);
	}

	private String retrieveLoggedInUserName() {
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		if (principal instanceof UserDetails)
			return ((UserDetails) principal).getUsername();
		return principal.toString();
	}
}
