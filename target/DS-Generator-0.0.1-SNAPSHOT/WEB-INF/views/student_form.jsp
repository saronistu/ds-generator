<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Edit student</h1>
		<form:form action="save?id=${student.id}" method="post" modelAttribute="student">
			<form:hidden path="id" />
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label>Nume</label>
						<form:input path="nume" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Nume familie dupa casatorie</label>
						<form:input path="numeFamilieDupaCasatorie" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Initiala tatalui/mamei</label>
						<form:input path="initialaTatalui" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Prenume</label>
						<form:input path="prenume" type="text" class="form-control"/>
					</div>
					<div class="dob-fields">
						<div class="form-group dob-field">
							<label>Anul nasterii</label>
							<form:input path="anulNasterii" type="text" class="form-control"/>
						</div>
						<div class="form-group dob-field">
							<label>Luna nasterii</label>
							<form:input path="lunaNasterii" type="text" class="form-control"/>
						</div>
						<div class="form-group dob-field">
							<label>Ziua nasterii</label>
							<form:input path="ziuaNasterii" type="text" class="form-control"/>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label>Locul nasterii</label>
						<form:input path="loculNasterii" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Numar matricol</label>
						<form:input path="numarMatricol" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>CNP</label>
						<form:input path="codNumericPersonal" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Anul inmatricularii</label>
						<form:input path="anulInmatricularii" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Anul absolvirii</label>
						<form:input path="anulAbsolvirii" type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Denumirea calificarii</label>
						<form:select path="domeniuStudii" type="text" class="form-control">
							<form:options items="${allDepartments}" />
						</form:select>
					</div>
				</div>
			</div>
			<input type="submit" class="btn btn-sitecolor" name="next" value="Next">
			<input type="submit" class="btn btn-success" name="save" value="Save">
		</form:form>
	</div>
<%@ include file="common/footer.jspf" %>