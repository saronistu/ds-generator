<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Enrol student to subject</h1>
			<form:form action="enrol-student" method="post" modelAttribute="enrol">
				<form:hidden path="id" />
				<div class="row">
					<div class="col">
<%--						<div class="form-group display-none">--%>
<%--							<form:label path="numarMatricol">Număr matricol:</form:label>--%>
<%--							<form:input path="numarMatricol" value="${numarMatricol}" type="number" class="form-control" required="required"/>--%>
<%--						</div>--%>
						<div class="form-group">
							<label>Materie:</label>
							<form:select path="nume" class="form-control">
								<form:options items="${allSubjects}" />
							</form:select>
						</div>
						<div class="form-group">
							<label>Ore C:</label>
							<form:input path="oreC" type="number" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Ore SLPP:</label>
							<form:input path="oreSLPP" type="number" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Notă sem 1:</label>
							<form:input path="notaSem1" type="text" class="form-control"/>
						</div>
					</div>
					<div class="col">
						<div class="form-group">
							<label>Notă sem 2:</label>
							<form:input path="notaSem2" type="text" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Credite sem 1:</label>
							<form:input path="crediteSem1" type="number" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Credite sem 2:</label>
							<form:input path="crediteSem2" type="number" class="form-control"/>
						</div>
						<div class="form-group">
							<label>An universitar: (yyyy/yyyy)</label>
							<form:input path="anUniversitar" type="text" class="form-control" required="required"/>
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-sitecolor" value="Enrol">
			</form:form>
	</div>
<%@ include file="common/footer.jspf" %>