<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
	<div class="container">
		<h1>Add a department</h1>
		<form:form action="add-departments" method="post" modelAttribute="department">
			<div class="form-group">
				<label>Nume:</label>
				<form:input path="nume" type="text" class="form-control"/>
			</div>
			<div class="form-group">
				<label>Facultate:</label>
				<form:select path="facultate" type="text" class="form-control">
					<form:option value="Teologie" />
					<form:option value="Management" />
				</form:select>
			</div>
			<input type="submit" class="btn btn-sitecolor" value="Add">
		</form:form>
	</div>
<%@ include file="common/footer.jspf" %>