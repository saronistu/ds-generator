<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<html class="index">
<body>
<header class="header" id="header">
    <div class="header__logo-box">
        <i class="feature-box__icon header__logo fas fa-file-pdf"></i>
    </div>
    <div class="header__text-box">
        <h1 class="heading-primary">
            <span class="heading-primary--main">DS-Generator</span>
            <span class="heading-primary--sub">Diploma Supplement Generator</span>
        </h1>
    </div>
</header>
<section class="section-features">
    <div class="row">
        <div class="col-1-of-3 feature-box-display-mobile">
            <div class="feature-box-title">
                <h1 class="heading-tertiary u-margin-bottom-small">The features of this web application</h1>
            </div>
        </div>
        <div class="col-1-of-3">
            <div class="feature-box-card">
                <i class="feature-box__icon fas fa-users"></i>
                <h3 class="heading-tertiary u-margin-bottom-small">User-based</h3>
                <p class="feature-box__text">
                    In order to access the features of this app, you have to have graduated at UEO and request an user account.
                </p>
            </div>
        </div>
        <div class="col-1-of-3 feature-box-display-desktop">
            <div class="feature-box-title">
                <h1 class="heading-tertiary u-margin-bottom-small">The features of this web application</h1>
            </div>
        </div>
        <div class="col-1-of-3">
            <div class="feature-box-card">
                <i class="feature-box__icon fas fa-terminal"></i>
                <h3 class="heading-tertiary u-margin-bottom-small">CSS at its finest</h3>
                <p class="feature-box__text">
                    The diploma page is precisely and painstakingly built with HTML and CSS.
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-1-of-3">
            <div class="feature-box-card">
                <i class="feature-box__icon fas fa-clone"></i>
                <h3 class="heading-tertiary u-margin-bottom-small">Duplication</h3>
                <p class="feature-box__text">
                    A student can be duplicated with all its info and grades making the process time saving.
                </p>
            </div>
        </div>
        <div class="col-1-of-3">
            <div class="feature-box-card">
                <i class="feature-box__icon fas fa-expand-alt"></i>
                <h3 class="heading-tertiary u-margin-bottom-small">Flexibility</h3>
                <p class="feature-box__text">
                    Any number of departments, subjects, and grades can be added for various scenarios.
                </p>
            </div>
        </div>
        <div class="col-1-of-3">
            <div class="feature-box-card">
                <i class="feature-box__icon fas fa-feather"></i>
                <h3 class="heading-tertiary u-margin-bottom-small">Lightweight</h3>
                <p class="feature-box__text">
                    The overall size of the application is under 100 MB (dependencies included) making it respond faster.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="section-form">
    <div class="row form-padding">
        <div class="book">
            <div class="book__form">
                <form action="#" class="form">
                    <div class=" u-margin-bottom-medium">
                        <h2 class="heading-secondary">
                            Request an account
                        </h2>
                    </div>
                    <div class="form__group">
                        <input type="text" class="form__input" placeholder="Full Name" id="name" required>
                        <label for="name" class="form__label">Full Name</label>
                    </div>
                    <div class="form__group">
                        <input type="email" class="form__input" placeholder="Email" id="email" required>
                        <label for="email" class="form__label">Email</label>
                    </div>
                    <div class="form__group">
                        <button type="submit" class="button button-form">Request</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="footer__logo-box">
        <img src="<x:url value="/resources/img/logo.png"/>" alt="that-photo" class="footer__logo"/>
    </div>
    <div class="row">
        <div class="col-1-of-2">
            <div class="footer__navigation">
                <ul class="footer__list">
                    <li class="footer__item"><a href="#" class="footer__link">Company</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Contact Us</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Careers</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Privacy</a></li>
                    <li class="footer__item"><a href="#" class="footer__link">Terms</a></li>
                </ul>
            </div>
        </div>
        <div class="col-1-of-2">
            <p class="footer__copyright">
                Build by <a href="#" class="footer__link">Saron Ciupe</a>. Copyright &copy; by Saron Ciupe.
            </p>
        </div>
    </div>
</footer>
<div class="popup" id="popup">
    <div class="popup__content">
            <h2 class="heading-secondary u-margin-bottom-small">Request Submitted</h2>
            <h3 class="heading-tertiary u-margin-bottom-small">Your request has been sent. We'll be in touch with you shortly with your account details.</h3>
            <a href="#popup" class="button button--white">Thank you!</a>
    </div>
</div>
</body>
</html>
<%@ include file="common/footer.jspf" %>